src package
===========

.. automodule:: src
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   src.analysis
   src.data
   src.models
   src.tools
   src.utility

Submodules
----------

src.config module
-----------------

.. automodule:: src.config
   :members:
   :undoc-members:
   :show-inheritance:
