.. DigiFall documentation master file, created by
   sphinx-quickstart on Tue Aug 24 08:59:20 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DigiFall's documentation!
====================================
This project aims to predict the fall-risk of community-dwelling dwelling elderly by using the home-care history and basic variables such as age or gender. There have both been attempts to  \\
use a recurrent neural network (RNN) in different ways (classifying falls binarily and predicting the number of falls using regression) and to use statistical regression methods to \\
to explain what makes up the risk of the elderly falling. Principal Component Analysis (PCA) have been tried in order to ease the learning and in order to do statistical regression.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
