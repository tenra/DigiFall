DigiFall
========================

DigiFall is a project that attempts to predict falls in the elderly population. Various machine learning models will be 
built using home-care observations in time along. Additionally statistical regression will be carried out to explain
the tendencies in the data more generally and assuming that the probability of falling follows a specific distribution.

The source code consists of two parts; a python package and a folder with R-code. The python-code have
been used to do all data preprocessing and all modelling of neural networks. The R-code have been used
to do all statistical regression. See the individual README-files for the two parts in order to properly
use the code and recreate the results.

Project Structure
========================

       ├── data
       │   ├── processed                    <- Data that is processed
       │   ├── raw                          <- Raw data as .xlsx files
       │   ├── README.md                    <- Description of the data and which observations are there
       │   └── ready                        <- Data ready for modelling
       │
       ├── docs                             <- Documentation of python code (_build/html/index.html to view)
       │
       ├── models                           <- Folder where trained models will be saved to and loaded from
       │
       ├── README.md                        <- This file
       │
       ├── references
       │   ├── care_types.txt               <- File with a list of all care types and their index
       │   ├── organisation_types.txt       <- File with a list of all organisation types and their index
       │   └── sex_types.txt                <- File with a list of all sex types
       │
       ├── src                              <- Python source-code used in this project
       │   ├── analysis                     <- Sub-package with all analysis
       │   ├── config.py                    <- Configuration used in this project
       │   ├── data                         <- Sub-package with all data-making
       │   ├── models                       <- Sub-package with all models used
       │   ├── README.md                    <- README-file for the python source code
       │   ├── tools                        <- Sub-package including tools
       │   └── utility                      <- Sub-package including utility
       │
       └── stat_reg
           ├── confidenceEmpCDF.R           <- The R code for generating confidence intervals for proportions
           ├── poisson_regression_model.R   <- The R code for running the statistical regression
           └── transparent_color.R          <- Code for making transparent colors in R
