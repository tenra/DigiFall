DigiFall R Source Code
===========================
The R source code have been used to do all the statistical regression carried out in the project. The code includes 3
scripts:

- *statistical_regression_model.R* - This file has the code for doing the analysis after having set the proper working
directory, the code can be run line by line producing statistics and plots. To do find the significant variables, an 
initial model with all parameters have been tried. Then insignificant variables are removed one at a time first by using
the *step*-function that bases which variable to remove on the log-likelihood estimates. After having applied the 
*step*-function, the remaining insignificant variables have been removed by hand according to the *p*-value using a
*z*-test. The *negative binomial distribution* have shown to be the most appropriate distribution of number of falls, 
hence the model "NGB" is the final model to be considered.
- *confidenceEmpCDF.R* - File made by Anders Reenberg Andersen. This file has a few functions for doing uncertainty estimates of proportions.
- *transparent_color.R* - This file has function for doing transparent colors for plotting (so that multiple things can
be plotted simultaneously)

#### Required R packages
In order to run the scripts the following packages need to be installed:

- MASS 7.3-53.1
- pscl 1.5.5
- tibble 3.1.0

## Course of Action
In order to do the statistical regression, the data to be used needs to be computed beforehand. The data can be computed
running the python script in *src / data / make_statreg_data.py*.

### Example Citizens
In order to show the results of applying the model to the example citizens, make the data using the python-script
*src / data / make_statred_example_data.py*. The end of the R script *statistical_regression_model.R* have the modelling
of these.