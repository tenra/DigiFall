The different datasets
======================
Short description of what each of the raw datasets contain:

 - **"83.xlsx"** - Citizens under the "service-law"
      - 31,214 observations on 8684 individuals and 8 different paragraphs
      - § 83 Kommunalbestyrelsen skal tilbyde 
         - 1) personlig hjælp og pleje,
         - 2) hjælp eller støtte til nødvendige praktiske opgaver i hjemmet og
         - 3) madservice.
      - § 84 Kommunalbestyrelsen skal tilbyde afløsning eller aflastning til ægtefælle, forældre eller andre nære pårørende, der passer en person med nedsat fysisk eller psykisk funktionsevne.
 - **"83a.xlsx"** - Citizens under the "rehabilitation law"
    - § 83a Kommunalbestyrelsen skal tilbyde et korterevarende og tidsafgrænset rehabiliteringsforløb til personer med nedsat funktionsevne, hvis rehabiliteringsforløbet vurderes at kunne forbedre personens funktionsevne og dermed nedsætte behovet for hjælp efter § 83, stk. 1. Vurderingen skal være individuel og konkret og tage udgangspunkt i modtagerens ressourcer og behov.
      - Stk. 2. Rehabiliteringsforløbet, jf. stk. 1, skal tilrettelægges og udføres helhedsorienteret og tværfagligt. Kommunalbestyrelsen skal fastsætte individuelle mål for rehabiliteringsforløbet i samarbejde med den enkelte modtager af forløbet.
 - **"86.xlsx"** - Citizens that need rehabilitation from illness
    - § 86 Kommunalbestyrelsen skal tilbyde **genoptræning** til afhjælpning af fysisk funktionsnedsættelse forårsaget af sygdom, der **ikke** behandles i tilknytning til en **sygehusindlæggelse**.
    - Stk. 2. Kommunalbestyrelsen skal tilbyde hjælp til at **vedligeholde fysiske eller psykiske færdigheder** til personer, som på grund af nedsat fysisk eller psykisk funktionsevne eller særlige sociale problemer har behov herfor.

 - **"140.xlsx"** - Citizens that need training after having been in the hospital
    - § 140  Kommunalbestyrelsen tilbyder vederlagsfri **genoptræning** til personer, der efter **udskrivning fra sygehus** har et lægefagligt begrundet behov for genoptræning, jf. § 84 om genoptræningsplaner.
 - **"AIR_FALD.xlsx"** - Fall observations used in the AIR-project
    - 24,834 fall observations on 4,098 individuals
 - **"Digi_06-2019_til_02-2021.xlsx"** - A list of all the home-care given since 2019 including how many minutes and what type
    - 365,086 home care observations on 809 individuals and 55 different types of care
 - **"Faldudredning.xlsx"** - A list of falls
    - 377 fall observations on 377 individuals
    - 274 of the individuals also have observations in the AIR fall data
   

### In other words

- §83 citizens need help to do normal things in the house
- §83a citizens get an offer for rehabilitation in order to be more independent
- §86 citizens need rehabilitation due to illness not caused by hospital admitance, but due to other reason such as social, psychological, or physical problems
- §140 citizens need rehabilitation after hospital admitance


# The home-care dataset

The home-care dataset contains weekly observations of how many minutes of a given type of home-care a person is given. The data contains:
- Observations from **week 23 of 2019** (medio-june) to **week 8 of 2021** (ultimo february).
- **55** different types of home care including general service (cooking, shopping etc) and personal hygiene (showering, discharging etc.)
- **809** individuals getting different kinds of help, from here:
  - **43** dies in the duration of the data
  - **497** falls at least once in the duration of the data
    
Some observations will need to be removed due to a too short history. At a prediction window of 10 weeks, this is around 100 (depending on the seed)


# The data sets ready for modelling

There are 3 (.pkl) files containing data ready for inserting into a network:
- *X.pkl* - One big 3-way array containing home-care observations for all citizens right-aligned and padded to be of equal length (shape N x T x P). The first 2 rows contain the year and the week-number.
- *G.pkl* - General variables for every citizen (containing sex, age at prediction time, and home-care organisation)
- *Y.pkl* - The event that happens on the event-time (falls, dies, or nothing)
