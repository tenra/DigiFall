"""
The src-package holds all the code used for data preprocessing and modelling.

- All the analysis is carried in the *analysis* sub-package.
- The models that are used are given in the *models* sub-package.
- The data preprocessing is carried out in the *data* sub-package.
- The tools for cleaning, reading, writing, visualizing and so forth are in the *tools* sub-package.
- The utilities for keeping track of citizens, PCA transformers, and more are given in the *utility* sub-package.
"""
import src.models
import src.utility
import src.data
import src.tools
