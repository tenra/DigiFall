"""
Function for traning and validating the RNN doing regression on the number of falls in the following period.
"""
import numpy as np
from src.models.RNNs import RNN_REG
from tensorflow import keras
from src.tools.data_loader import Data
import matplotlib.pyplot as plt


if __name__ == '__main__':
    data = Data()
    data.load_data('pca_reg')
    validation_split = 0.2
    train_samples = int((1 - validation_split) * len(data.X))
    perm = np.random.permutation(len(data.X))
    train_ix, test_ix = perm[:train_samples], perm[train_samples:]

    model = RNN_REG(300, 300, dropout=0.7)
    optimizer = keras.optimizers.Adam(learning_rate=0.0001)
    model.compile(loss='mean_squared_error')
    model.fit(x=[data.X[train_ix], data.G[train_ix]], y=data.Y[train_ix],
              validation_data=([data.X[test_ix], data.G[test_ix]], data.Y[test_ix]), epochs=50)
    hist = model.history

    fig, axes = plt.subplots(2, 2)
    axes[0, 0].plot(hist.history['loss'])
    axes[0, 0].set(title='Loss')
    axes[0, 1].plot(hist.history['val_loss'])
    axes[0, 1].set(title='Val. Loss')
    axes[1, 0].scatter(data.Y[test_ix], model.call([data.X[test_ix], data.G[test_ix]]))
    axes[1, 0].set(xlim=[0, 5], ylim=[0, 5])
    axes[1, 0].plot([0, 10], [0, 10], color='black')
    fig.show()
