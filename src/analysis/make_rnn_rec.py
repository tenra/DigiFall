"""
Training and testing the recurrent-faller model (outcome is binary by the fall-intensity). Using cross-validation to \\
plot the mean ROC and PR curves.
"""
import numpy as np
from time import time
from sklearn.model_selection import StratifiedKFold
from src.models.RNNs import RNN
from tensorflow import keras
from src.tools.data_loader import Data
import src.tools.visualizer as vis
import matplotlib.pyplot as plt
from tensorflow.keras.metrics import AUC

if __name__ == '__main__':
    seed = 42
    EPOCHS = 100

    # Loading the data
    data = Data()
    data.load_data('rec')
    data.normalize()
    histories = []

    fig, axes = plt.subplots(1, 2, figsize=(12, 12))
    mean_fpr, mean_rec = np.linspace(0, 1, 100), np.linspace(0, 1, 100)
    fall_aucs, die_aucs = [], []
    fall_praucs, die_praucs = [], []
    fall_tprs, die_tprs = [], []
    fall_precs, die_precs = [], []
    overall_res, best_confs, best_models = [], [], []

    # Running the outer cross-validation loop
    outer_folds = 5
    cv_outer = StratifiedKFold(n_splits=outer_folds, shuffle=True, random_state=1)
    t = time()
    for j, (train_ix, test_ix) in enumerate(cv_outer.split(data.X, data.Y)):
        print(f"Starting {j + 1}. fold...    ", end="")
        X_train, X_test = data.X[train_ix, :, :], data.X[test_ix, :, :]
        G_train, G_test = data.G[train_ix, :], data.G[test_ix, :]
        Y_train, Y_test = data.Y[train_ix], data.Y[test_ix]

        model = RNN(100, 100, dropout=0.7)

        optimizer = keras.optimizers.Adam(learning_rate=0.0001)
        model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=[AUC(name='auc'), AUC(name='prauc', curve='PR')])
        hist = model.fit(x=[X_train, G_train], y=Y_train, validation_data=([X_test, G_test], Y_test), epochs=EPOCHS, verbose=0)
        histories.append(hist)

        # Doing the visualisation
        preds = model.predict([X_test, G_test])
        true = Y_test

        ### Falling part
        # ROC
        fall_aucs.append(hist.history['val_auc'][-1])
        fall_tprs.append(vis.plot_roc_curve(preds, true,
                                            label=f"F{j} auc: {fall_aucs[-1]:.2f}", ax=axes[0],
                                            interpolate_fpr=mean_fpr))
        # PR
        fall_praucs.append(hist.history['val_prauc'][-1])
        fall_precs.append(vis.plot_pr_curve(preds, true,
                                            label=f"F{j} PRauc: {fall_praucs[-1]:.2f}",
                                            ax=axes[1], interpolate_rec=mean_rec))

        print(f"Finished {j + 1}. fold")

    print(f"Overall time was {time() - t:.2f} seconds or {(time() - t) / 60:.2f} minutes")

    # Finishing the plots:
    # ROC curves
    vis.generate_mean_roc(mean_fpr=mean_fpr, interp_tprs=fall_tprs, ax=axes[0], title="Falling", aucs=fall_aucs)

    p = np.mean(data.Y)
    vis.generate_mean_pr(mean_rec=mean_rec, interp_precs=fall_precs, ax=axes[1],
                         praucs=fall_praucs, p=p, title=None)

    fig.show()
