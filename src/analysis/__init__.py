"""
This sub-package holds the files in which all the analysis (training and validation) of different models have taken \\
place.

- The 'first-fall' outcome have been model in *make_rnn.py*, and nested cross-validation have been attempted used to \
find the best model for each fold in *make_cross_validated.py*
- The 'recurrent-faller' (binary based on fall-intensity) outcome have been modelled in *make_rnn_rec.py*, and using \
the PCA scores instead of actual home-care observations in *make_rnn_pca.py*
- The 'recurrent-faller' regression modelling the number of falls in the following period have been modelled in \
*make_rnn_reg.py*. This uses the PCA scores.
"""