"""
In this file nested cross-validation is used to assess the best model for each of the folds in the outer loop. All \\
the configuration in "param_grid" will be assessed, hence the optimal models with be a combination of these. The \\
outcome that is modelled in this file is the original 'first'fall' outcome using the actual home-care observations.
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from time import time
from src.tools.data_loader import Data
import src.tools.visualizer as vis
from src.models.RNNs import create_model
from sklearn.model_selection import StratifiedKFold, ParameterGrid

# %% Running the cross validation
if __name__ == '__main__':
    seed = 42
    EPOCHS = 10

    data = Data()
    data.load_data('first')
    data.normalize()

    # Need to make another version of Y in order to do stratified cross validation
    YC = np.zeros(len(data.Y))
    for i, y in enumerate(data.Y):
        if np.array_equal(y, np.array([[0, 1], [1, 0]])):
            YC[i] = 1
        elif np.array_equal(y, np.array([[1, 0], [0, 1]])):
            YC[i] = 2

    # The parameter grid
    param_grid = dict()
    param_grid['GRU_units'] = [2, 5]
    param_grid['GRU1_units'] = [0, 2, 5]
    param_grid['common_lin_units'] = [0, 2, 5]
    param_grid['lin_fall'] = [5, 10]
    param_grid['lin_die'] = [2, 5]
    PG = ParameterGrid(param_grid)

    # Initialize all lists and plots
    fig, axes = plt.subplots(2, 2, figsize=(10, 10))
    mean_fpr, mean_rec = np.linspace(0, 1, 100), np.linspace(0, 1, 100)
    fall_aucs, die_aucs = [], []
    fall_praucs, die_praucs = [], []
    fall_tprs, die_tprs = [], []
    fall_precs, die_precs = [], []
    overall_res, best_confs, best_models = [], [], []

    # Running the outer cross-validation loop
    outer_folds = 5
    cv_outer = StratifiedKFold(n_splits=outer_folds, shuffle=True, random_state=1)
    t = time()
    for j, (train_ix, test_ix) in enumerate(cv_outer.split(data.X, YC)):
        X_train, X_test = data.X[train_ix, :, :], data.X[test_ix, :, :]
        G_train, G_test = data.G[train_ix, :], data.G[test_ix, :]
        Y_train, Y_test = data.Y[train_ix, :, :], data.Y[test_ix, :, :]
        YC_train, YC_test = YC[train_ix], YC[test_ix]

        inner_folds = 3
        cv_inner = StratifiedKFold(n_splits=inner_folds, shuffle=True, random_state=1)
        results = pd.DataFrame(PG)
        num_parms = len(results.keys())
        results['fold_mean'] = np.zeros(len(results))

        for i, (train_ix2, test_ix2) in enumerate(cv_inner.split(X_train, YC_train)):
            X_train2, X_test2 = X_train[train_ix2, :, :], X_train[test_ix2, :, :]
            G_train2, G_test2 = G_train[train_ix2, :], G_train[test_ix2, :]
            Y_train2, Y_test2 = Y_train[train_ix2, :, :], Y_train[test_ix2, :, :]

            fall_res, die_res = [], []
            fold_res = []
            for conf in ParameterGrid(param_grid):
                model = create_model(**conf)

                hist = model.fit(x=[X_train2, G_train2], epochs=EPOCHS,
                                 y={'falling': Y_train2[:, 0, 1], 'dying': Y_train2[:, 1, 1]},
                                 validation_data=([X_test2, G_test2],
                                                  {'falling': Y_test2[:, 0, 1], 'dying': Y_test2[:, 1, 1]}),
                                 verbose=0)

                fall_res.append(hist.history['val_falling_prauc'][-1])
                die_res.append(hist.history['val_dying_prauc'][-1])
                fold_res.append((fall_res[-1] + die_res[-1]) / 2)
            print(f"Fold ({j},{i}) just done")
            results[f"fold{i}_fall_prauc"] = fall_res
            results[f"fold{i}_die_prauc"] = die_res
            results[f"fold{i}_mean:prauc"] = fold_res
            results['fold_mean'] += np.array(fold_res) / inner_folds

        results = results.sort_values('fold_mean', ascending=False)
        best_conf = dict(results.iloc[0][:num_parms])
        best_model = create_model(**best_conf)
        hist = best_model.fit(x=[X_train, G_train], epochs=EPOCHS,
                              y={'falling': Y_train[:, 0, 1], 'dying': Y_train[:, 1, 1]},
                              validation_data=([X_test, G_test], {'falling': Y_test[:, 0, 1], 'dying': Y_test[:, 1, 1]}),
                              verbose=0)

        # Doing the visualisation
        preds = best_model.predict([X_test, G_test])
        fall_preds, fall_true = preds['falling'], Y_test[:, 0, 1]
        die_preds, die_true = preds['dying'], Y_test[:, 1, 1]

        ### Falling part
        # ROC
        fall_aucs.append(hist.history['val_falling_auc'][-1])
        fall_tprs.append(vis.plot_roc_curve(fall_preds, fall_true,
                                            label=f"F{j} auc: {fall_aucs[-1]:.2f}", ax=axes[0, 0],
                                            interpolate_fpr=mean_fpr))
        # PR
        fall_praucs.append(hist.history['val_falling_prauc'][-1])
        fall_precs.append(vis.plot_pr_curve(fall_preds, fall_true,
                                            label=f"F{j} PRauc: {fall_praucs[-1]:.2f}",
                                            ax=axes[1, 0], interpolate_rec=mean_rec))

        ### Dying part
        # ROC
        die_aucs.append(hist.history['val_dying_auc'][-1])
        die_tprs.append(vis.plot_roc_curve(die_preds, die_true,
                                           label=f"F{j} auc: {die_aucs[-1]:.2f}", ax=axes[0, 1],
                                           interpolate_fpr=mean_fpr))

        # PR
        die_praucs.append(hist.history['val_dying_prauc'][-1])
        die_precs.append(vis.plot_pr_curve(die_preds, die_true,
                                           label=f"F{j} PRauc: {die_praucs[-1]:.2f}", ax=axes[1, 1],
                                           interpolate_rec=mean_rec))

        # Adding models to list
        best_models.append(best_model)
        best_confs.append(best_conf)
        overall_res.append((hist.history['val_falling_prauc'][-1] + hist.history['val_dying_prauc'][-1]) / 2)
    print(f"Overall time was {time()-t:.2f} seconds or {(time()-t)/60:.2f} minutes")

    # Finishing the plots:
    # ROC curves
    vis.generate_mean_roc(mean_fpr=mean_fpr, interp_tprs=fall_tprs, ax=axes[0, 0], title="Falling", aucs=fall_aucs)
    vis.generate_mean_roc(mean_fpr=mean_fpr, interp_tprs=die_tprs, ax=axes[0, 1], title="Dying", aucs=die_aucs)

    fall_p, die_p = np.mean(data.Y[:, 0, 1]), np.mean(data.Y[:, 1, 1])
    vis.generate_mean_pr(mean_rec=mean_rec, interp_precs=fall_precs, ax=axes[1, 0],
                         praucs=fall_praucs, p=fall_p, title=None)
    vis.generate_mean_pr(mean_rec=mean_rec, interp_precs=die_precs, ax=axes[1, 1],
                         praucs=die_praucs, p=die_p, title=None)

    fig.show()
