"""
Training and validation of the initial model with multiple outcomes (falling and dying). This model attempts to model \\
the first fall (vs death), and AUCs (and PR-AUCs) for both outcomes will be calculated. Cross-validation is used to \\
assess the model performance.
"""
from src.models.RNNs import multioutcome_RNN
from tensorflow import keras
import numpy as np
from src.tools.data_loader import Data
from src.tools.preprocessor import upsample
import src.tools.visualizer as vis
from tensorflow.keras.metrics import AUC
from sklearn.model_selection import StratifiedKFold
import matplotlib.pyplot as plt
from time import time

if __name__ == '__main__':
    # Cross validate performance
    seed = 42
    EPOCHS = 30

    data = Data()
    data.load_data('first')
    data.normalize()

    # Need to make another version of Y in order to do stratified cross validation
    YC = np.zeros(len(data.Y))
    for i, y in enumerate(data.Y):
        if np.array_equal(y, np.array([[0, 1], [1, 0]])):
            YC[i] = 1
        elif np.array_equal(y, np.array([[1, 0], [0, 1]])):
            YC[i] = 2

    fig, axes = plt.subplots(2, 2, figsize=(12, 12))
    mean_fpr, mean_rec = np.linspace(0, 1, 100), np.linspace(0, 1, 100)
    fall_aucs, die_aucs = [], []
    fall_praucs, die_praucs = [], []
    fall_tprs, die_tprs = [], []
    fall_precs, die_precs = [], []
    overall_res, best_confs, best_models = [], [], []

    # Running the outer cross-validation loop
    outer_folds = 5
    cv_outer = StratifiedKFold(n_splits=outer_folds, shuffle=True, random_state=1)
    t = time()
    for j, (train_ix, test_ix) in enumerate(cv_outer.split(data.X, YC)):
        print(f"Starting {j + 1}. fold...    ", end="")
        X_train, X_test = data.X[train_ix, :, :], data.X[test_ix, :, :]
        G_train, G_test = data.G[train_ix, :], data.G[test_ix, :]
        Y_train, Y_test = data.Y[train_ix, :, :], data.Y[test_ix, :, :]
        YC_train, YC_test = YC[train_ix], YC[test_ix]

        # Upsampling
        X_train, G_train, Y_train = upsample(X_train, G_train, Y_train)

        # Making the model
        model = multioutcome_RNN(0, 10, 3, 3, 1, dropout=0.7)
        metrics = ['accuracy', AUC(name='auc'), AUC(name='prauc', curve="PR")]
        optimizer = keras.optimizers.SGD(learning_rate=0.001)
        losses = {'falling': 'binary_crossentropy', 'dying': 'binary_crossentropy'}
        loss_weights = {'falling': 1.0, 'dying': 1.0}
        model.compile(optimizer=optimizer, loss=losses, loss_weights=loss_weights, metrics=metrics)

        hist = model.fit(x=[X_train, G_train], epochs=EPOCHS,
                         y={'falling': Y_train[:, 0, 1], 'dying': Y_train[:, 1, 1]},
                         validation_data=([X_test, G_test], {'falling': Y_test[:, 0, 1], 'dying': Y_test[:, 1, 1]}),
                         verbose=0)

        # Doing the visualisation
        preds = model.predict([X_test, G_test])
        fall_preds, fall_true = preds['falling'], Y_test[:, 0, 1]
        die_preds, die_true = preds['dying'], Y_test[:, 1, 1]

        ### Falling part
        # ROC
        fall_aucs.append(hist.history['val_falling_auc'][-1])
        fall_tprs.append(vis.plot_roc_curve(fall_preds, fall_true,
                                            label=f"F{j} auc: {fall_aucs[-1]:.2f}", ax=axes[0, 0],
                                            interpolate_fpr=mean_fpr))
        # PR
        fall_praucs.append(hist.history['val_falling_prauc'][-1])
        fall_precs.append(vis.plot_pr_curve(fall_preds, fall_true,
                                            label=f"F{j} PRauc: {fall_praucs[-1]:.2f}",
                                            ax=axes[1, 0], interpolate_rec=mean_rec))

        ### Dying part
        # ROC
        die_aucs.append(hist.history['val_dying_auc'][-1])
        die_tprs.append(vis.plot_roc_curve(die_preds, die_true,
                                           label=f"F{j} auc: {die_aucs[-1]:.2f}", ax=axes[0, 1],
                                           interpolate_fpr=mean_fpr))

        # PR
        die_praucs.append(hist.history['val_dying_prauc'][-1])
        die_precs.append(vis.plot_pr_curve(die_preds, die_true,
                                           label=f"F{j} PRauc: {die_praucs[-1]:.2f}", ax=axes[1, 1],
                                           interpolate_rec=mean_rec))

        # Adding models to list
        overall_res.append((hist.history['val_falling_prauc'][-1] + hist.history['val_dying_prauc'][-1]) / 2)
        print(f"Finished {j + 1}. fold")
    print(f"Overall time was {time() - t:.2f} seconds or {(time() - t) / 60:.2f} minutes")

    # Finishing the plots:
    # ROC curves
    vis.generate_mean_roc(mean_fpr=mean_fpr, interp_tprs=fall_tprs, ax=axes[0, 0], title="Falling", aucs=fall_aucs)
    vis.generate_mean_roc(mean_fpr=mean_fpr, interp_tprs=die_tprs, ax=axes[0, 1], title="Dying", aucs=die_aucs)

    fall_p, die_p = np.mean(data.Y[:, 0, 1]), np.mean(data.Y[:, 1, 1])
    vis.generate_mean_pr(mean_rec=mean_rec, interp_precs=fall_precs, ax=axes[1, 0],
                         praucs=fall_praucs, p=fall_p, title=None)
    vis.generate_mean_pr(mean_rec=mean_rec, interp_precs=die_precs, ax=axes[1, 1],
                         praucs=die_praucs, p=die_p, title=None)

    fig.show()
