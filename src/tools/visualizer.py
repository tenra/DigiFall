import matplotlib.colors as base_colors
from typing import List, Union
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import numpy as np
from pathlib import Path
from sklearn.metrics import roc_auc_score, roc_curve, precision_recall_curve, precision_recall_fscore_support, \
    confusion_matrix, auc

COLORS = list(base_colors.BASE_COLORS)


def plot_roc_curve(preds: np.ndarray, y_true: np.ndarray, label: str, ax: plt.Axes, drop_intermediate: bool = False,
                   interpolate_fpr: Union[np.ndarray, None] = None) -> np.ndarray:
    """
    Functions that plots the reciever-operator curve given predictions and true values. An ax object must be given as\
    input and the curve will be plotted on this and given the label also given to the function. If a curve is wanted\
    on a specific set of false-positive rates (x-axis) a numpy array with these can be given and the interpolated\
    true positive rates will be returned.

    :param preds: Predictions (probability of 1 for each observation)
    :param y_true: True values (binary)
    :param label: Label to give the curve
    :param ax: An ax element on which the curve will be drawn
    :param drop_intermediate: Settings this true give a lower resolution on the curve
    :param interpolate_fpr: A numpy array of false-positive rates on which to interpolate the true-positive rates.
    :return: (fpr, tpr, th) or interpolated tprs if interpolate_fpr is given
    """
    fpr, tpr, th = roc_curve(y_true, preds, drop_intermediate=drop_intermediate)
    ax.plot(fpr, tpr, alpha=0.5, label=label)
    if interpolate_fpr is None:
        return fpr, tpr, th
    interp_tpr = np.interp(interpolate_fpr, fpr, tpr)
    interp_tpr[0] = 0.0
    return interp_tpr


def generate_mean_roc(mean_fpr: np.ndarray, interp_tprs: np.ndarray, ax: plt.Axes, aucs: np.ndarray,
                      title: Union[str, None] = None):
    """
    Function for editing an ax element with multiple ROC-curves drawn on it and plotting the mean of the ROC-curves\
    which must be given as an inputs. The mean curve will be drawn on the ax element and the figure will be given the\
    desired title.

    :param mean_fpr: The false positive rates used to generate the interpolated true positive rates
    :param interp_tprs: The interpolated true positives rates for the individual lines
    :param ax: The ax object on which to draw the mean PR-curve
    :param aucs: List of AUCs of the individual curves
    :param title: The title of the plot
    """
    mean_tpr = np.mean(interp_tprs, axis=0)
    mean_tpr[-1] = 1.0

    # The uncertainty
    std_tpr = np.std(interp_tprs, axis=0)
    tpr_upper = np.minimum(mean_tpr + std_tpr, 1)
    tpr_lower = np.maximum(mean_tpr - std_tpr, 0)
    mean_auc = auc(mean_fpr, mean_tpr)
    std_auc = np.std(aucs)

    # Plotting
    ax.plot(mean_fpr, mean_tpr, label=rf"Mean. auc: {mean_auc:.2f} $\pm$ {std_auc:.2f}", linewidth=2.0, color="black")
    ax.fill_between(mean_fpr, tpr_lower, tpr_upper, color="grey", alpha=0.2, label=rf"$\pm$ 1 std.0")
    ax.plot([0, 1], [0, 1], ls="--", color='black', label="Random")
    if title is not None:
        ax.set(title=title)
    ax.set(xlabel=fr'1 - Specificity', ylabel='Sensitivity')
    ax.legend()


def plot_pr_curve(preds: np.ndarray, y_true: np.ndarray, label: str, ax: plt.Axes,
                  interpolate_rec: Union[np.ndarray, None] = None) -> np.ndarray:
    """
    Functions that plots the precision-recall curve given predictions and true values. An ax object must be given as\
    input and the curve will be plotted on this and given the label also given to the function. If a curve is wanted\
    on a specific set of recalls (x-axis) a numpy array with these can be given and the interpolated\
    precisions will be returned.

    :param preds: Predictions (probability of 1 for each observation)
    :param y_true: True values (binary)
    :param label: Label to give the curve
    :param ax: An ax element on which the curve will be drawn
    :param interpolate_rec:
    :return: (rec, prec, th) or interpolated precisions if interpolate_rec is given
    """
    prec, rec, th_pr = precision_recall_curve(y_true, preds)
    ax.plot(rec, prec, alpha=0.5, label=label)
    if interpolate_rec is None:
        return rec, prec, th_pr
    interp_prec = np.interp(interpolate_rec, np.flip(rec), np.flip(prec))
    interp_prec[0] = 1.0
    return interp_prec


def generate_mean_pr(mean_rec: np.ndarray, interp_precs: np.ndarray, ax: plt.Axes, praucs: np.ndarray, p: float,
                     title: Union[str, None] = None):
    """
    Function for editing an ax element with multiple PR-curves drawn on it and plotting the mean of the PR-curves\
    which must be given as an inputs. The mean curve will be drawn on the ax element and the figure will be given the\
    desired title. The grey area is the uncertainty of the mean curve.

    :param mean_rec: The recall values used to generate the interpolated precisions
    :param interp_precs: Interpolated precisions of each of the PR-curves
    :param ax: An ax object on which to draw the line
    :param praucs: List of precision-recall AUCs for the individual curves
    :param p: The true proportion of positives in the data
    :param title: The title to give the plot
    """
    mean_prec = np.mean(interp_precs, axis=0)
    mean_prec[0] = 1.0

    # The uncertainty
    std_prec = np.std(interp_precs, 0)
    prec_upper = np.minimum(mean_prec + std_prec, 1)
    prec_lower = np.maximum(mean_prec - std_prec, 0)
    mean_prauc = auc(mean_rec, mean_prec)
    std_prauc = np.std(praucs)

    # Plotting
    ax.plot(mean_rec, mean_prec, label=rf"Mean. prauc: {mean_prauc:.2f} $\pm$ {std_prauc:.2f}", linewidth=2.0,
            color="black")
    ax.fill_between(mean_rec, prec_lower, prec_upper, color="grey", alpha=0.2, label=rf"$\pm$ 1 std.")
    ax.plot([0, 1], [p, p], color='black', ls="--", label="Random")
    if title is not None:
        ax.set(title=title)
    ax.set(xlabel='Recall', ylabel='Precision')
    ax.legend()


def compare_ROC_PR(preds: List[List[float]], y_true: List[int], names: List[str], show_opt: bool = False,
                   beta: int = 1, drop_intermediate: bool = False):
    """
    Function that plots and compares multiple model on the same test data. 'preds' is a list of prediction vectors of \
    the predictions corresponding to the given true outcomes. The curves are all plotted together and the legend is \
    including their AUC for both the ROC and the PR plots.

    :param preds: List of prediction vectors for each of the models to be compared
    :param y_true: The true y-values
    :param names: The name of each of the models to be compared (same length as 'preds')
    :param show_opt: Boolean specifying if the optimal points wrt to the geometric mean and the Youden-index \
    respectively should be shown for the ROC-curve and the optimal points wrt the F-beta-score and the Fowlkes-Mallows\
    index should be shown for the PR-curve.
    :param beta: The beta-value for the F-beta statistic. (recall is worth beta times more than precision)
    :param drop_intermediate: True to make the curves smoother
    :return: figure with 2 subplots - a ROC curve and a PR curve
    """
    pos_rate = np.mean(y_true)
    fig, (roc_plot, pr_plot) = plt.subplots(1, 2, figsize=(10, 6))
    roc_lines = [roc_plot.plot([0, 1], [0, 1], ls="--", label="Random")[0]]
    pr_lines = [pr_plot.plot([0, 1], [pos_rate, pos_rate], ls="--", label="Random")[0]]

    for i, pred in enumerate(preds):
        # The ROC curve
        fpr, tpr, th_roc = roc_curve(y_true, pred, drop_intermediate=drop_intermediate)
        auc = roc_auc_score(y_true, pred)
        roc_lines.append(roc_plot.plot(fpr, tpr, ls="solid", color=COLORS[i], label=f"{names[i]} (AUC: {auc:.2f})")[0])

        if show_opt:
            # Geom. mean of tpr and fpr
            _, opt_G_idx = g_score(tpr, fpr)

            # Youden-index
            _, opt_J_idx = j_score(tpr, fpr)

            roc_plot.scatter(fpr[opt_G_idx], tpr[opt_G_idx], s=70, marker="o", color=COLORS[i])
            roc_plot.scatter(fpr[opt_J_idx], tpr[opt_J_idx], s=70, marker="^", color=COLORS[i])

        # The Precision-recall curve
        prec, rec, th_pr = precision_recall_curve(y_true, pred)
        pr_lines.append(pr_plot.plot(rec, prec, ls="solid", color=COLORS[i], label=f"{names[i]}")[0])

        if show_opt:
            # F-score
            _, opt_F_idx = f_score(prec, rec, beta)

            # Fowlkes-Mallows index
            _, opt_FM_idx = fm_score(prec, rec)

            pr_plot.scatter(rec[opt_F_idx], prec[opt_F_idx], s=70, marker="s", color=COLORS[i])
            pr_plot.scatter(rec[opt_FM_idx], prec[opt_FM_idx], s=200, marker="*", color=COLORS[i])

    roc_plot.set(title="Receiver-Operator Curve", xlabel="1 - Specificity", ylabel="Sensitivity")
    if show_opt:
        roc_lines.append(
            Line2D([0], [0], marker="o", markersize=8, color="w", markerfacecolor="black", label="Geom. mean"))
        roc_lines.append(
            Line2D([0], [0], marker="^", markersize=8, color="w", markerfacecolor="black", label="Youden index"))
    roc_plot.set_xlim([0, 1])
    roc_plot.set_ylim([0, 1])
    roc_plot.legend(handles=roc_lines, loc="lower right")

    pr_plot.set(title="Precision-Recall Curve", xlabel="Recall", ylabel="Precision", ylim=(0, 1))
    if show_opt:
        pr_lines.append(
            Line2D([0], [0], marker="s", markersize=8, color="w", markerfacecolor="black", label=f"F_{beta}-value"))
        pr_lines.append(
            Line2D([0], [0], marker="*", markersize=15, color="w", markerfacecolor="black", label="Fowlkes-Mallows"))
    pr_plot.set_xlim([0, 1])
    pr_plot.set_ylim([0, 1])
    pr_plot.legend(handles=pr_lines, loc="lower left")

    fig.subplots_adjust(left=0.075, right=0.99)
    fig.show()
    return fig


def show_ROC_PR(pred: List[float], y_true: List[int], name: str, show_opt: bool = False, beta: float = 1.0,
                show_thresholds: bool = False, drop_intermediate: bool = False, thresh_holds: List[float] = None) \
        -> plt.Figure:
    """
    Function for plotting the reciever-operator curve and the precision-recall curve potentially including optimal \\
    thresholds.

    :param pred: The predicted probabilities
    :param y_true: The true (binary) outcomes
    :param name: Name of the model used to generate the predictions
    :param show_opt: Boolean specifying whether to show the optimal thresholds or not
    :param beta: beta-value to be used in the F-beta statistic (how many times more important recall is than precision)
    :param show_thresholds: Boolean specifying whether to show threshold points along the line
    :param drop_intermediate: True gives fewer points
    :param thresh_holds: The thresholds to be plotted along the line (defaults to (0.05, 0.1, 0.25, 0.5, 0.75, 0.9, 0.95))
    :return: matplotlib figure object
    """
    if thresh_holds is None and show_thresholds is True:
        thresh_holds = [0.05, 0.1, 0.25, 0.5, 0.75, 0.9, 0.95]

    pos_rate = np.mean(y_true)
    fig, (roc_plot, pr_plot) = plt.subplots(1, 2, figsize=(10, 6))
    roc_plot.plot([0, 1], [0, 1], ls="--", label="Random")
    pr_plot.plot([0, 1], [pos_rate, pos_rate], ls="--", label="Random")

    # ROC-curve
    fpr, tpr, th_roc = roc_curve(y_true, pred, drop_intermediate=drop_intermediate)
    auc = roc_auc_score(y_true, pred)
    roc_plot.plot(fpr, tpr, ls="solid", color="black", label=f"{name} (AUC: {auc:.2f})")
    if show_opt:
        # Youden index
        _, opt_J_idx = j_score(tpr, fpr)
        opt_J_th = th_roc[opt_J_idx]

        # Geometric mean of tpr and fpr
        _, opt_G_idx = g_score(tpr, fpr)
        opt_G_th = th_roc[opt_G_idx]

        roc_plot.scatter(fpr[opt_G_idx], tpr[opt_G_idx], s=70, marker="o", color="blue",
                         label=f"Geom. mean (th: {opt_G_th:.2f})")
        roc_plot.scatter(fpr[opt_J_idx], tpr[opt_J_idx], s=70, marker="^", color="orange",
                         label=f"Youden-index (th: {opt_J_th:.2f})")

    # PR-curve
    prec, rec, th_pr = precision_recall_curve(y_true, pred)
    pr_plot.plot(rec, prec, ls="solid", color="black", label=f"{name}")
    if show_opt:
        # F-score
        _, opt_F_idx = f_score(prec, rec, beta)
        opt_F_th = th_pr[opt_F_idx]

        # The Fowlkes-Mallows index
        _, opt_FM_idx = fm_score(prec, rec)
        opt_FM_th = th_pr[opt_FM_idx]

        pr_plot.scatter(rec[opt_F_idx], prec[opt_F_idx], s=70, marker="s", color="red",
                        label=f"F{beta}-score (th: {opt_F_th:.2f})")
        pr_plot.scatter(rec[opt_FM_idx], prec[opt_FM_idx], s=200, marker="*", color="green",
                        label=f"FM-index (th: {opt_FM_th:.2f})")

    if show_thresholds:
        for threshold in thresh_holds:
            # Roc_plot
            idx_roc = np.argwhere(th_roc < threshold)[0]
            roc_plot.scatter(fpr[idx_roc], tpr[idx_roc], marker="o", color="black")
            roc_plot.annotate(f"{threshold:.2f}", (fpr[idx_roc] + 0.01, tpr[idx_roc] - 0.03))

            # PR_plot
            idx_pr = np.argwhere(th_pr > threshold)[0]
            pr_plot.scatter(rec[idx_pr], prec[idx_pr], marker="o", color="black")
            pr_plot.annotate(f"{threshold:.2f}", (rec[idx_pr] + 0.01, prec[idx_pr] + 0.01))
        roc_plot.scatter(fpr[idx_roc], tpr[idx_roc], marker="o", color="black", label="Example thresholds")
        pr_plot.scatter(rec[idx_pr], prec[idx_pr], marker="o", color="black", label="Example thresholds")

    roc_plot.set_xlim([0, 1])
    roc_plot.set_ylim([0, 1])
    roc_plot.legend(loc="lower right")
    roc_plot.set(title="Receiver-Operator Curve", xlabel="1 - Specificity", ylabel="Sensitivity")

    pr_plot.set_xlim([0, 1])
    pr_plot.set_ylim([0, 1])
    pr_plot.legend(loc="lower left")
    pr_plot.set(title="Precision-Recall Curve", xlabel="Recall", ylabel="Precision")

    fig.subplots_adjust(left=0.075, right=0.99)
    fig.show()
    return fig


def show_calibrations(preds: List[List[float]], y_true: List[int], names: List[str], n_bins: int = 10,
                      xrange: List[float] = [0, 1], equally_sized: bool = False) -> plt.Figure:
    """
    Function for plotting the calibrations plot for different models

    :param preds: list of lists of predictions of the same test data
    :param y_true: the true outcome values
    :param names: list of names of the model
    :param n_bins:
    :param xrange:
    :param equally_sized:
    :return:
    """
    fig, ax = plt.subplots(1, 1)
    plots = [ax.plot([0, 1], [0, 1], ls="--", label="Perfect Calibration")[0]]
    breaks, means, ratios = [], [], []
    for j, pred in enumerate(preds):
        pred_range = np.all([pred >= xrange[0], pred <= xrange[1]], axis=0)
        pred = pred[pred_range]
        this_y = y_true[pred_range]
        preds_sorted = np.sort(pred, axis=0)
        if equally_sized:
            batch_size = len(pred) // n_bins
            this_breaks = [0]
            for i in range(1, n_bins):
                this_breaks.append(preds_sorted[batch_size * i])
            this_breaks.append(1.)
            breaks.append(this_breaks)
        else:
            breaks.append(np.linspace(xrange[0], xrange[1], n_bins + 1))
        batch_preds, batch_obs = [], []
        for i in range(0, n_bins):
            batch = np.all([pred >= breaks[-1][i], pred < breaks[-1][i + 1]], axis=0)
            batch_preds.append(np.mean(pred[batch]))
            batch_obs.append(np.mean(this_y[batch]))
        ax.scatter(batch_preds, batch_obs, marker="o", color=COLORS[j])
        ax.plot(batch_preds, batch_obs, color=COLORS[j])
        plots.append(Line2D([0], [0], marker="o", color=COLORS[j], label=names[j]))
    ax.legend(handles=plots)
    ax.set_xlim(xrange)
    ax.set(title=f"Calibration plot with batches of {'equal size' if equally_sized else 'equal width':s}",
           xlabel="Mean predicted value", ylabel="True proportion of positives")
    fig.show()
    return fig


def show_decision_curves(preds: List[List[float]], y_true: List[int], names: List[str],
                         xrange: List[float] = [0, .99], n_bins: int = 50, harm: float = 0) -> plt.Figure:
    """
    Function for plotting decision curves for different models. Harm to be subtracted from the net-benefit

    :param preds: list of lists of predictions
    :param y_true: list of true outcomes
    :param names: list of name of the models
    :param xrange: the range to plot on the x-axis
    :param n_bins: the number of bins to be used
    :param harm:
    :return: matplotlib figure object
    """
    n, pp = len(y_true), np.mean(y_true)
    fig, ax = plt.subplots(1, 1)
    pt = np.linspace(xrange[0], xrange[1], n_bins)
    # Treatment for none
    ax.plot([0, 1], [0, 0], ls="--", label="Treatment for none")
    # Treatment for all
    CM = confusion_matrix(y_true, np.ones(n))
    TP, FP = CM[1, 1], CM[0, 1]
    benefit_all = (TP - FP * (pt / (1 - pt))) / n
    ax.plot(pt, benefit_all, ls="solid", color="black", label="Treatment for all")
    # Perfect classifier
    ax.plot([0, 1], [pp, pp], ls="-.", color="C0", label="Perfect classification")

    for i, pred in enumerate(preds):
        TPs, FPs = [], []
        for th in pt:
            y_pred = pred > th
            CM = confusion_matrix(y_true, y_pred)
            TPs.append(CM[1, 1])
            FPs.append(CM[0, 1])
        net_benefit = (np.array(TPs) - np.array(FPs) * (pt / (1 - pt))) / n - harm
        ax.plot(pt, net_benefit, color=f"C{i + 1}", label=names[i])
    ax.legend()
    ax.set_xlim(xrange)
    ax.set_ylim([0 - pp * 0.2, pp * 1.2])
    ax.set(title="Decision curve", xlabel="Threshold percentage", ylabel="Net benefit")
    fig.show()
    return fig


def show_DCA_TPs_removed(preds: List[float], y_true: List[int], xrange: List[float] = [0, .99],
                         n_bins: int = 50, harm: float = 0) -> plt.Figure:
    """
    Function for plotting a single decision curve and for showing the benefit of treatment for all and treatment for \\
    none.

    :param preds: list of predictions
    :param y_true: list of true values
    :param xrange: the range of x
    :param n_bins: the number of bins to be used
    :param harm: the harm to subtract from the net-benefit
    :return: matplotlib figure object
    """
    n, pp = len(y_true), np.mean(y_true)
    fig, ax = plt.subplots(1, 1)
    ax2 = ax.twinx()
    pt = np.linspace(xrange[0], xrange[1], n_bins)
    # Treatment for none
    ax.plot([0, 1], [0, 0], ls="--", label="Treatment for none")
    # Treatment for all
    CM = confusion_matrix(y_true, np.ones(n))
    TP, FP = CM[1, 1], CM[0, 1]
    benefit_all = (TP - FP * (pt / (1 - pt))) / n
    ax.plot(pt, benefit_all, ls="solid", color="black", label="Treatment for all")
    # Perfect classifier
    ax.plot([0, 1], [pp, pp], ls="-.", color="C0", label="Perfect classification")
    TPs, FPs = [], []
    for th in pt:
        y_pred = preds > th
        CM = confusion_matrix(y_true, y_pred)
        TPs.append(CM[1, 1])
        FPs.append(CM[0, 1])
    net_benefit = (np.array(TPs) - np.array(FPs) * (pt / (1 - pt))) / n - harm
    ax.plot(pt, net_benefit, color="blue", label="Net-benefit")
    rem_TPs = (net_benefit - benefit_all) / (pt / (1 - pt))
    ax2.plot(pt, rem_TPs * 100, color="red", label="Model-advantage")
    ax2.set_ylabel("TPs avoided pr. 100", color="red")
    ax.set_ylabel("Net-benefit", color="blue")
    ax.set_xlabel("Threshold percantage")
    ax.set_xlim(xrange)
    ax.set_ylim([0 - pp * 0.2, pp * 1.2])
    fig.legend()
    fig.show()
    return fig


def j_score(tpr: np.ndarray, fpr: np.ndarray) -> (float, int):
    """
    Calculating the optimal Youden index and value given tpr and fpr pairs.
    :param tpr: True Positive Rate - Array with different TPR values
    :param fpr: False Positive Rate - Array with different FPR values
    :return: (optimal Youden score, optimal index)
    """
    J_score = tpr - fpr
    opt_idx = np.argmax(J_score)
    return J_score[opt_idx], opt_idx


def g_score(tpr, fpr) -> (float, int):
    """
    Calculating the optimal value and index of the geometric mean between sensitivity (tpr) and false positive rate.
    :param tpr: True Positive Rate (Sensitivity) as a numpy array
    :param fpr: False Positive Rate as a numpy array
    :return: (optimal geom. mean score, optimal index)
    """
    G_score = np.sqrt(tpr * (1 - fpr))
    opt_idx = np.argmax(G_score)
    return G_score[opt_idx], opt_idx


def f_score(prec: np.ndarray, rec: np.ndarray, beta: float = 1.0) -> (float, int):
    """
    Calculating the optimal value and index of the beta-adjusted f-score. The beta value is the relative importance of
    the sensitivity to the precision. (I.e. capturing the positives vs. precision of positive predictions)
    :param prec: Precision as a numpy array
    :param rec: Recall (sensitivity) as a numpy array
    :param beta: Relative importance of recall to precision. Floating point value. Default value: 1
    :return: (optimal beta-adjusted f_score, optimal index)
    """
    F_score = (1 + beta ** 2) * (rec * prec) / (beta ** 2 * prec + rec)
    opt_idx = np.argmax(F_score)
    return F_score[opt_idx], opt_idx


def fm_score(prec: np.ndarray, rec: np.ndarray) -> (float, int):
    """
    Calculating the optimal value and index of the Fowlkes-Mallows index (geometric mean of precision and recall).
    :param prec: Precision as a numpy array
    :param rec: Recall (sensitivity) as a numpy array
    :return: (optimal value of the fowlkes-malles score, optimal index)
    """
    FM_score = np.sqrt(rec * prec)
    opt_idx = np.argmax(FM_score)
    return FM_score[opt_idx], opt_idx


def MCC_score(y_true: np.ndarray, preds: np.ndarray, saveAt: Path = None) -> (np.ndarray, float):
    """
    Calculates and plots the values of the Matthews Correlation Coefficient (phi-coefficient), and shows the optimum.

    :param y_true: The real labels
    :param preds:  The predictions as probabilities
    :param saveAt: Directory to output file if applicable
    :return: A vector with MCC values
    """
    P = int(np.sum(y_true))
    N = len(y_true) - P
    fpr, tpr, th = roc_curve(y_true, preds, drop_intermediate=False)
    TP = tpr[1:-1] * P
    FP = fpr[1:-1] * N
    FN = P - TP
    TN = N - FP
    MCCs = (TP * TN - FP * FN) / (((TP + FP) ** 0.5) * ((TP + FN) ** 0.5) * ((TN + FP) ** 0.5) * ((TN + FN) ** 0.5))
    opt_idx = np.argmax(MCCs)
    fig, ax = plt.subplots(1, 1)
    ax.plot(th[1:-1], MCCs, label="Phi-coefficient")
    print(th[opt_idx + 1], MCCs[opt_idx])
    ax.scatter(th[opt_idx + 1], MCCs[opt_idx], color="black", label=f"Optimal threshold: {th[opt_idx + 1]:.2f}")
    ax.legend()
    fig.show()
    if saveAt is not None:
        fig.savefig(saveAt)
    return MCCs, th
