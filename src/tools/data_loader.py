import numpy as np
from typing import Union
from src.tools.reader import load_pickle
from pathlib import Path
import src.config as cf


class Data:
    """
    Class for data that is ready for modelling. Can be initialized with data for X, G and Y, but can also be \
    initialized empty.

    :param X: The data in time - shape N x T x P. Default is None
    :param G: The general data vector with information such as sex, age (at prediction), week (of prediction), \
    organisation etc. Default is None
    :param Y: The outcome data encoded to be (None, Falls, Dies). Default is None
    """

    def __init__(self, X: Union[np.ndarray, None] = None, G: Union[np.ndarray, None] = None,
                 Y: Union[np.ndarray, None] = None, normalized: bool = False):
        self.X = X
        self.G = G
        self.Y = Y
        self.normalized = normalized

    def load_data(self, which: str):
        """
        Loading the data ready for modelling into this data instance.

        :param which: String that is either "rec", "pca", "pca_reg" or "first" depending on which data set to load
        """
        if which == "first":
            filenames = cf.FILENAMES_READY_FIRST
        elif which == 'rec':
            filenames = cf.FILENAMES_READY_REC
        elif which == 'pca':
            filenames = cf.FILENAMES_READY_PCA
        elif which == 'pca_reg':
            filenames = cf.FILENAMES_READY_PCA_REG
        self.X = load_pickle(Path.joinpath(cf.READY_DATA_DIR, filenames['X'])).astype('float')
        self.G = load_pickle(Path.joinpath(cf.READY_DATA_DIR, filenames['G'])).astype('float')
        self.Y = load_pickle(Path.joinpath(cf.READY_DATA_DIR, filenames['Y'])).astype('float')

    def make_split(self, p: float = 0.2, seed: int = None):
        """
        Make two datasets randomly from this where the 2nd has the fraction p of the observations and the first has \
        the fraction 1-p of the observations.

        :param p: fraction of observations to go on the 2nd dataset
        :param seed: seed parameter if the same numbers are wanted again
        :return: 2 data instances where the fraction of observations in the first is 1-p, and the 2nd is p,
        """
        if self.X is None:
            raise ValueError("Data object is empty")
        if self.normalized is False:
            print("Warning!:  Data have not been normalized yet!")
        N = len(self.X)
        if seed is not None:
            np.random.seed(seed)
        test_inds = np.random.choice(np.arange(N), size=int(N * p), replace=False)
        train_inds = np.array([i for i in np.arange(N) if i not in test_inds])

        train_data = Data(self.X[train_inds], self.G[train_inds], self.Y[train_inds], self.normalized)
        test_data = Data(self.X[test_inds], self.G[test_inds], self.Y[test_inds], self.normalized)
        return train_data, test_data

    def __str__(self):
        if self.X is None:
            return "Empty data set"
        return f"Data set with {len(self.X)} observations"

    def normalize(self):
        if self.normalized:
            raise ValueError("Data have already been normalized!")
        if self.X is None:
            raise ValueError("Empty data instance")
        factors = np.expand_dims(np.expand_dims(self.X.max(axis=0).max(axis=0), 0), 0) + 1
        factors[-1] = 1
        self.G[:, 0] /= 53  # Normalizing the week number
        self.G[:, 1] /= self.G[:, 1].max()
        self.G[:, -1] /= 100
        self.X = self.X / factors
        self.normalized = True

    def remove_dead(self):
        """
        Removing the individuals that die without falling before.
        """
        removed = 0
        for i in range(len(self.X) - 1, -1, -1):
            if self.Y[i, 1, 1] == 1:
                self.X = np.delete(self.X, i, axis=0)
                self.G = np.delete(self.G, i, axis=0)
                self.Y = np.delete(self.Y, i, axis=0)
                removed += 1
        print(f"Removed {removed} observations. The dataset now contains {len(self.X)} observations")

    def remove_obs(self, index: int):
        """
        Removing the observation at a given index.

        :param index:
        """
        self.X = np.delete(self.X, index, axis=0)
        self.G = np.delete(self.G, index, axis=0)
        self.Y = np.deleta(self.Y, index, axis=0)
