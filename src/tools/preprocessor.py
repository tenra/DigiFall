from src.utility.data_utils import Citizen, date_after, year_week_diff, get_later_week
from typing import Tuple, Union
import numpy as np
import src.config as cf


def make_y_first(citizen: Citizen) -> (Tuple[Tuple[int, int], Tuple[int, int]], Tuple[int, int]):
    """
    Returning the y for a given citizen based their fall history and whether they are considered dead.

    :param citizen: A citizen object of the given citizen
    :return: a tuple of format (none, falls, dies) with a one in the one that happens first and a time0 for when that \
    happens
    """
    # Assessing if the citizen falls at all
    falling = len(citizen.falls) > 0

    if citizen.dead:
        if falling:
            # If the fall date is before the last observation for the given person he is falling
            if date_after(citizen.last_week, citizen.falls[0]):
                return ((0, 1), (1, 0)), citizen.falls[0]  # falls
            else:
                return ((1, 0), (0, 1)), citizen.last_week  # dies
        else:
            return ((1, 0), (0, 1)), citizen.last_week  # dies
    else:
        if falling:
            # Falls can happen after home-care data runs out why we need to make sure that we have all data
            # before the prediction window (i.e. fall is not happening too far in the future)
            if year_week_diff(cf.LAST_WEEK, citizen.falls[0]) >= -cf.PREDICTION_WINDOW:
                return ((0, 1), (1, 0)), citizen.falls[0]  # falls
            else:
                return ((1, 0), (1, 0)), get_later_week(citizen.dates[np.random.randint(len(citizen.dates))],
                                                        cf.PREDICTION_WINDOW)  # Nothing happens
        else:
            return ((1, 0), (1, 0)), get_later_week(citizen.dates[np.random.randint(len(citizen.dates))],
                                                    cf.PREDICTION_WINDOW)  # Nothing happens


def make_y_rec(citizen: Citizen) -> (Union[Tuple[int, int], None], Union[int, None], Union[float, None]):
    """
    Make the outcomes for the people falling again. Making the prediction after the cf.NUM_FALL_PRED'th fall. \\
    Returning 1 if the fall intensity is above the threshold and 0 if not. Outputs the fall intensity.

    :param citizen: Citizen object
    :return: prediction time, outcome value, intensity
    """
    follow_up_len = max(year_week_diff(citizen.dates[-1], citizen.falls[cf.NUM_FALL_PRED - 1]),
                        year_week_diff(citizen.falls[-1], citizen.falls[cf.NUM_FALL_PRED - 1]))
    future_falls = len(citizen.falls) - cf.NUM_FALL_PRED

    pred_time = tuple(citizen.falls[cf.NUM_FALL_PRED - 1])

    if follow_up_len == 0:
        return None, None, None
    intensity = future_falls / follow_up_len
    y = 1 if intensity > (cf.INTENSITY_THRESHOLD / 52) else 0

    return pred_time, y, intensity


def make_x_first(citizen: Citizen, event_time: Tuple[int, int]) -> Tuple[Union[None, np.ndarray], Tuple[int, int]]:
    """
    Making the x data array for the citizen based on the time 0 for that person, also removing date-variables so it \
    is only home-care data. Also returning prediction time.

    :param citizen: a citizen object
    :param event_time: a time of the event to be modelled given as (year, week)
    :return: numpy array with every row being a weekly observation and the prediction time
    """
    pred_time = get_later_week(event_time, -cf.PREDICTION_WINDOW)
    if year_week_diff(pred_time, citizen.first_week) < 0 or year_week_diff(pred_time, citizen.last_week) > 0:
        return None, pred_time
    home_care = citizen.home_care[:(citizen.date_dict[pred_time] + 1), 2:]
    home_care = np.concatenate((home_care, np.ones((len(home_care), 1))), axis=1)
    return home_care, pred_time


def make_x_rec(citizen: Citizen, pred_time: Tuple[int, int]) -> Union[None, np.ndarray]:
    """
    Making the x data array for the citizen based on the time 0 for that person, also removing date-variables so it \
    is only home-care data. Also returning prediction time.

    :param citizen: a citizen object
    :param pred_time: a time of the event to be modelled given as (year, week)
    :return: numpy array with every row being a weekly observation and the prediction time
    """
    if year_week_diff(pred_time, citizen.first_week) < 0 or year_week_diff(pred_time, citizen.last_week) > 0:
        return None
    home_care = citizen.home_care[:(citizen.date_dict[pred_time] + 1), 2:]
    home_care = np.concatenate((home_care, np.ones((len(home_care), 1))), axis=1)
    return home_care


def upsample(X, G, Y, shuffle=True):
    """
    Function for upsampling the given data in order to deal with the great unbalance in outcomes and to ease the \
    learning process. The observations with\
    respectively positive outcomes for falling or dying are sampled randomly until the number is the same as for the\
    negative outcomes.

    :param X: home_care array
    :param G: array of constants
    :param Y: array of outcomes
    :param shuffle: Boolean that is true if the data should be shuffled before returning. Default: True
    :return: Upsampled arrays of X, G, and Y
    """
    X_train = []
    G_train = []
    Y_train = []

    # Upsampling
    X_falls = X[Y[:, 0, 1] == 1]
    G_falls = G[Y[:, 0, 1] == 1]

    X_dies = X[Y[:, 1, 1] == 1]
    G_dies = G[Y[:, 1, 1] == 1]

    n, n_fall, n_die = len(X), len(X_falls), len(X_dies)

    # Upsampling falling observations
    for i in range(n - n_fall):
        ind = np.random.randint(0, n_fall)
        X_train.append(X_falls[ind])
        G_train.append(G_falls[ind])
        Y_train.append(np.array([[0, 1], [1, 0]]))
        # Upsampling dying observations
    for i in range(n - n_die):
        ind = np.random.randint(0, n_die)
        X_train.append(X_dies[ind])
        G_train.append(G_dies[ind])
        Y_train.append(np.array([[1, 0], [0, 1]]))

    X_train = np.concatenate((X, np.stack(X_train)), axis=0)
    G_train = np.concatenate((G, np.stack(G_train)), axis=0)
    Y_train = np.concatenate((Y, np.stack(Y_train)), axis=0)

    if shuffle is True:
        ind = np.arange(3 * n - n_die - n_fall)
        np.random.shuffle(ind)
        return X_train[ind], G_train[ind], Y_train[ind]
    else:
        return X_train, G_train, Y_train
