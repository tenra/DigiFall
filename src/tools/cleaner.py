import pandas as pd


def clean_care_type(df: pd.DataFrame) -> pd.DataFrame:
    """
    Cleans special danish characters

    :param df: the data frame that has the 'care_type' parameter
    :return: clean dataframe
    """
    # Care types
    care_types = list(df.care_type)
    for i, string in enumerate(care_types):
        care_types[i] = clean_string(string)
    df.care_type = care_types
    # Organisation types
    organisation_types = list(df.Organisation)
    for i, string in enumerate(organisation_types):
        organisation_types[i] = clean_string(string)
    df.Organisation = organisation_types
    return df


def clean_string(string: str) -> str:
    string = string.replace('Ã¸', 'ø')
    string = string.replace('Ã¦', 'æ')
    string = string.replace('Ã¥', 'å')
    return string
