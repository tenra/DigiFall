from pathlib import Path
import pandas as pd


class raw_data_parser:
    """
    Class for passing the raw data

    :param path: Path to the folder where the raw data is located
    """

    def __init__(self, path):
        self.path = path
        self.converters = {'BorgerID': str}

    def parse_raw_home_care(self, filename) -> pd.DataFrame:
        """
        Parser for the DigiRehab home-care data that holds the number of minutes of home care received for each type
        of care. The parameters are:
         - Year
         - week
         - care type
         - Organisation (Private / municipal)
         - minutes - Minutes of home care given that week
         - num_cares - how many visits of the given type carried out in the given week
         - sex
         - ID
         - birth_year

        :param filename: The name of the file with the data
        :return: A panda dataframe
        """
        X = pd.read_excel(Path.joinpath(self.path, filename), converters=self.converters)
        X = X.drop('Afregning tid kategori', axis=1)
        X = X.drop('LeverandÃ¸r navn', axis=1)
        X = X.dropna(axis=0)
        X = X.rename(columns={'Ã…r uge': 'year', 'Ugenummer': 'week', 'Ydelse navn' : 'care_type',
                              'Organisation (niveau 03)': 'Organisation', 'Leveret tid (minutter)': 'minutes',
                              'Antal ydelser': 'num_cares', 'Køn': 'sex', 'BorgerID': 'ID', 'Født': 'birth_year'})
        X.year = [int(x.split('-')[0]) for x in X.year]
        return X

    def parse_fall_data(self, filename, full_dates=False) -> pd.DataFrame:
        """
        Passing the fall data from DigiRehab. The data has the following parameters:
         - year - of fall
         - week - of fall
         - sex
         - ID
         - birth_year
        If the full_dates parameters is true, the year and week parameters will be a single date parameter

        :param filename: The name of the file
        :param full_dates: Boolen specifying whether to return the full dates instead of year and week
        :return: A pandas dataframe
        """
        X = pd.read_excel(Path.joinpath(self.path, filename), converters=self.converters)
        X = X.rename(columns={'Dato': 'date' if full_dates else 'year', 'Køn': 'sex', 'BorgerID': 'ID',
                              'Født': 'birth_year'})
        if full_dates is False:
            X['week'] = [date.week for date in X.year]
            X.year = [date.year for date in X.year]
        return X

    def parse_AIR_falls(self, filename, full_dates=False) -> pd.DataFrame:
        """
        Passing the AIR fall data. The output will have the following parameters:
         - year - of fall
         - week - of fall
         - sex
         - ID
         - birth_year

        :param filename: Name of file
        :param full_dates: Boolen specifying whether to return the full dates instead of year and week
        :return: Pandas dataframe
        """
        X = pd.read_excel(Path.joinpath(self.path, filename), converters=self.converters)
        X = X.drop('Hvornår faldt borgeren?', axis=1)
        X = X.drop(24834, axis=1)
        X = X.drop('Hvor skete faldet', axis=1)
        X = X.drop('Omstændigheder', axis=1)
        X = X.drop('Anvendte borgeren hjælpemidler', axis=1)
        X = X.drop('Hjælp til at komme op?', axis=1)
        X = X.drop('Er borgeren kommet til skade', axis=1)
        X = X.rename(columns={'Dato': 'date' if full_dates else 'year', 'Køn': 'sex', 'Født': 'birth_year',
                              'BorgerID': 'ID'})
        if full_dates is False:
            X['week'] = [date.week for date in X.year]
            X.year = [date.year for date in X.year]
        return X

    def parse_paragraph_data(self, filename) -> pd.DataFrame:
        """
        Parser for the paragraph data. All the paragraph files have the following:
         - paragraph
         - sex
         - ID
         - birth_year
        The §83 file has an additional parameter that explain the type of homecare received by the inhabitant
         - care_type

        :param filename: Name of the file
        :return: Pandas dataframe
        """
        X = pd.read_excel(Path.joinpath(self.path, filename), converters=self.converters)
        X = X.rename(columns={'Paragraf': 'paragraph', 'Ydelse navn': 'care_type', 'BorgerID': 'ID',
                              'Født': 'birth_year'})
        return X
