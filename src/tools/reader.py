import pickle
from typing import Any, List
from pathlib import Path
import src.config as cf
from src.utility.data_utils import Citizen


def load_pickle(path: Path) -> Any:
    """
    Loads the pickled object at the location given.

    :param path: Path (including the file itself)
    :return: obj
    """
    file_handler = open(path, 'rb')
    obj = pickle.load(file_handler)
    file_handler.close()
    return obj


def load_citizens() -> (List[Citizen], dict, dict):
    """
    Function that loads the citizen list and the neccesary dictionaries.

    :return: citizen list, sex dictionary, organisation dictionary
    """
    citizens = load_pickle(Path.joinpath(cf.PROCESSED_DATA_DIR, 'data_by_citizen.pkl'))
    sex_dict = load_pickle(Path.joinpath(cf.PROCESSED_DATA_DIR, cf.FILENAMES_REF['sex']))
    organisation_dict = load_pickle(Path.joinpath(cf.PROCESSED_DATA_DIR, cf.FILENAMES_REF['organisation']))
    return citizens, sex_dict, organisation_dict
