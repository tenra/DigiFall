"""
This file cleans the data and makes list both the list of citizens and the different dictionaries to be used in \\
further analysis and saves it all the the processed data folder.
"""
import numpy as np
from src.tools import parser, cleaner, writer
import src.config as cf
from src.utility.data_utils import make_date_dict, make_type_dict, Citizen
from pathlib import Path
from time import time
from tqdm import tqdm

if __name__ == '__main__':
    P = parser.raw_data_parser(cf.RAW_DATA_DIR)

    # Loading the data
    t = time()
    print("-- Loading data --")
    home_care = cleaner.clean_care_type(P.parse_raw_home_care(cf.FILENAMES_RAW['home_care']))
    fall = P.parse_fall_data(cf.FILENAMES_RAW['fall'])
    fall_AIR = P.parse_AIR_falls(cf.FILENAMES_RAW['fall_AIR'])

    # p83 = P.parse_paragraph_data(cf.FILENAMES_RAW['P83'])
    # p83a = P.parse_paragraph_data(cf.FILENAMES_RAW['P83a'])
    # p86 = P.parse_paragraph_data(cf.FILENAMES_RAW['P86'])
    # p140 = P.parse_paragraph_data(cf.FILENAMES_RAW['P140'])
    print(f"Loading the data took {time() - t:.2f} seconds. ")

    # Cleaning
    print(f"-- Now cleaning the data --")
    print(f"Gathering data for each citizen and recoding variables (one observation per week per citizen)")

    unique_IDs = home_care.ID.unique()

    # Finding unique care types
    care_types = home_care.care_type.unique()
    care_dict = make_type_dict(care_types)
    writer.write_file(list(np.arange(care_types.shape[0])), list(care_types),
                      path=Path.joinpath(cf.REFERENCES_DIR, 'care_types.txt'), column_names=['index', 'type'])

    # Making the date dictionary
    dates, date_dict = make_date_dict(home_care)

    # Finding unique organisation types
    organisation_types = home_care.Organisation.unique()
    organisation_dict = make_type_dict(organisation_types)
    writer.write_file(list(np.arange(organisation_types.shape[0])), list(organisation_types),
                      path=Path.joinpath(cf.REFERENCES_DIR, 'organisation_types.txt'), column_names=['index', 'type'])

    # Finding unique sex types
    sex_types = home_care.sex.unique()
    sex_dict = make_type_dict(sex_types)
    writer.write_file(list(np.arange(sex_types.shape[0])), list(sex_types),
                      path=Path.joinpath(cf.REFERENCES_DIR, 'sex_types.txt'), column_names=['index', 'type'])

    # Making the citizen list
    citizens = []
    for id_ in tqdm(unique_IDs):
        this_citizen = Citizen(home_care[home_care.ID == id_], care_dict)  # Can add minute limit
        # Falls from "FALD_AIR"
        if cf.INCLUDE_AIR_FALLS:  # Does not add the falls that are already present from the "fall" data set
            this_falls = fall_AIR[['year', 'week']][fall_AIR.ID == id_].to_numpy()
            for year, week in this_falls:
                this_citizen.add_fall((year, week))
        # Falls from "Faldudredning"
        this_falls = fall[['year', 'week']][fall.ID == id_].to_numpy()
        for year, week in this_falls:
            this_citizen.add_fall((year, week), check_duplicates=True)
        citizens.append(this_citizen)

    # Saving the objects in order to load them faster in the future
    writer.pickle_obj(Path.joinpath(cf.PROCESSED_DATA_DIR, 'data_by_citizen.pkl'), citizens)
    writer.pickle_obj(Path.joinpath(cf.PROCESSED_DATA_DIR, 'all_weeks.pkl'), dates)
    writer.pickle_obj(Path.joinpath(cf.PROCESSED_DATA_DIR, 'care_types.pkl'), care_types)
    writer.pickle_obj(Path.joinpath(cf.PROCESSED_DATA_DIR, 'care_dict.pkl'), care_dict)
    writer.pickle_obj(Path.joinpath(cf.PROCESSED_DATA_DIR, 'organisation_dict.pkl'), organisation_dict)
    writer.pickle_obj(Path.joinpath(cf.PROCESSED_DATA_DIR, 'sex_dict.pkl'), sex_dict)
