"""
This file makes the data for carrying out regression using an RNN. The PCA scores are used as the history instead of \\
the actual observations. The outcome is now the number of falls during following, specified period of time.
"""
from src.tools import reader, writer, preprocessor
from src.utility.data_utils import year_week_diff, get_later_week
from src.utility.PCA import PrincipalComponentAnalysis
from tensorflow.keras.preprocessing.sequence import pad_sequences
import numpy as np
from pathlib import Path
import src.config as cf
from tqdm import tqdm

if __name__ == "__main__":
    NUM_components = 21  # Number of components from the PCA should be used
    citizens, sex_dict, organisation_dict = reader.load_citizens()

    # Doing a PCA analysis on all the home-care observations simultaneously
    HC = [c.home_care[:, 2:] for c in citizens]
    HC = np.concatenate(HC)
    HC = np.delete(HC, 51, axis=1)  # Removing column 51 (Forflytning og Mobilisering) because we have no obs.

    pca = PrincipalComponentAnalysis(30)
    pca.fit(HC)
    pca.draw_scree_plots(K=50)
    pca.choose_num_comp(NUM_components)
    comps = pca.get_components()

    X, G, Y = [], [], []

    for citizen in tqdm(citizens):

        citizen.fill_care_holes()

        if len(citizen.falls) < cf.NUM_FALL_PRED:
            continue

        pred_time = tuple(citizen.falls[cf.NUM_FALL_PRED - 1])
        end_date = get_later_week(pred_time, 12)  # Date 12 weeks in the future
        if year_week_diff(end_date, cf.LAST_WEEK) > 0:
            continue
        y = len(citizen.falls_between(pred_time, end_date))  # How many falls the next 3 months
        if y > cf.NUM_FALLS_MAX:
            continue

        x = preprocessor.make_x_rec(citizen, pred_time)
        if x is None:
            continue

        x = np.delete(x[:, :-1], 51, axis=1)

        xs = []
        for week in x:
            xs.append(pca.make_score(week))

        X.append(np.stack(xs))
        Y.append(y)
        G.append(citizen.get_constants(pred_time, sex_dict, organisation_dict))

    X = pad_sequences(X, dtype='float')
    Y = np.stack(Y)
    G = np.stack(G)

    print(f"\nData set made with a total of {len(Y)} observations of which {np.sum(Y > 0)} are considered to be 'fallers'. "
          f"\n")
    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'X_pca_reg.pkl'), X)
    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'G_pca_reg.pkl'), G)
    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'Y_pca_reg.pkl'), Y)
    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'PCA_transformer_reg.pkl'), pca)
