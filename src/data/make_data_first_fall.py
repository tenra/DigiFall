"""
This file makes the data ready for the analysis trying to predict the first fall of the citizens.
"""
from src.tools import reader, writer, preprocessor
from tensorflow.keras.preprocessing.sequence import pad_sequences
import numpy as np
from pathlib import Path
import src.config as cf
from tqdm import tqdm

if __name__ == "__main__":
    print("Preprocessing the dataset")
    citizens, sex_dict, organisation_dict = reader.load_citizens()
    X, Y, G = [], [], []  # The Ys are given as (falls, dies)

    for citizen in tqdm(citizens):
        # Making sure there are no holes in the weekly observations
        citizen.fill_care_holes()

        # Skipping people with only one observation
        if len(citizen.dates) == 1:
            continue

        # Make y and time 0
        y, event_time = preprocessor.make_y_first(citizen)
        Y.append(y)

        # Make x and check if there is enough information (prediction_time being after the first week and before the
        # last)
        x, pred_time = preprocessor.make_x_first(citizen, event_time)
        if x is None:
            Y.pop(-1)
            continue
        X.append(x)

        # Adding constants sex, age at prediction, week of prediction, and care organisation
        G.append(citizen.get_constants(pred_time, sex_dict=sex_dict, organisation_dict=organisation_dict))

    X = pad_sequences(X)
    Y = np.array(Y)
    G = np.array(G)
    print(f"\nData set made with a total of {len(Y)} observations of which {np.sum(Y[:,0, 1])} are falling, "
          f"{np.sum(Y[:,1, 1])} are dying\n")
    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'X_first.pkl'), X)
    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'G_first.pkl'), G)
    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'Y_first.pkl'), Y)
