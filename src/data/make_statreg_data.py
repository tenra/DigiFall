"""
This file makes the data to be used for statistical regression in R. The output will be one big dataframe with all \\
observations including the outcome values. The outcome is the number of falls in the following period (see \\
configurations). The PCA scores will be used as the home-care observations - both the values in the last week \\
(prediction time) and the values from a number of weeks ago (see configurations).
"""
import pandas as pd

import src.tools.reader
from src.tools import reader
from src.utility.data_utils import year_week_diff, get_later_week
from src.utility.PCA import PrincipalComponentAnalysis
import numpy as np
from pathlib import Path
import src.config as cf
from tqdm import tqdm

if __name__ == "__main__":
    citizens, sex_dict, organisation_dict = reader.load_citizens()

    # Doing a PCA analysis on all the home-care observations simultaneously
    HC = [c.home_care[:, 2:] for c in citizens]
    HC = np.concatenate(HC)
    HC = np.delete(HC, 51, axis=1)  # Removing column 51 (Forflytning og Mobilisering) because we have no obs.

    pca = PrincipalComponentAnalysis(30)
    pca.fit(HC)
    pca.draw_scree_plots(K=50)
    pca.choose_num_comp(cf.NUM_components)
    comps = pca.get_components()

    X, Y = [], []

    for citizen in tqdm(citizens):

        citizen.fill_care_holes()

        if len(citizen.falls) < 2:
            continue

        # Gathering multiple observations per person if applicable
        date = citizen.first_week
        while True:
            future_falls = citizen.falls_between(date)
            if len(future_falls) < 2:
                break
            date = future_falls[1]  # The second future fall
            time = year_week_diff(date, future_falls[0])

            end_date = get_later_week(date, cf.LOOK_FORWARD)
            if year_week_diff(end_date, cf.LAST_WEEK) > 0:
                break
            num_falls_period = len(citizen.falls_between(date, end_date))

            try:
                home_care = np.delete(citizen.home_care[citizen.date_dict[tuple(date)], 2:], 51)
            except KeyError:
                home_care = np.zeros(54)
            home_care = pca.make_score(home_care)
            try:
                home_care2 = np.delete(citizen.home_care[citizen.date_dict[get_later_week(date, -cf.LOOK_BACK)], 2:], 51)
            except KeyError:
                home_care2 = np.zeros(54)
            home_care2 = pca.make_score(home_care2)

            this_x = np.append(np.append(np.append(time, citizen.get_constants(date, sex_dict,
                                                                               organisation_dict)[1:-2]),
                                         home_care), home_care2)
            X.append(this_x)
            Y.append(num_falls_period)
            date = end_date
    X = np.stack(X)
    Y = np.stack(Y)

    columns = [f'W1_PC{i+1}' for i in range(cf.NUM_components)] + [f'W2_PC{i+1}' for i in range(cf.NUM_components)]
    df = pd.DataFrame(X, columns=['time', 'age', 'Male', 'Female']+columns)
    df['Y'] = Y
    save_path = Path.joinpath(cf.READY_DATA_DIR, 'poisson_data.csv')
    df.to_csv(save_path, sep=";")
    print(f'CSV file saved to {save_path}')
