"""
This file makes the data for modelling the recurrent-faller outcome using the actual home-care observations. The \\
outcome is binary and true if the fall-intensity in the following period is high enough (see configurations).
"""
from src.tools import reader, writer, preprocessor
from tensorflow.keras.preprocessing.sequence import pad_sequences
import numpy as np
from pathlib import Path
import src.config as cf
from tqdm import tqdm

if __name__ == "__main__":
    print("Preprocessing the data set ... ")
    citizens, sex_dict, organisation_dict = reader.load_citizens()

    X, G, Y, I = [], [], [], []
    for citizen in tqdm(citizens):

        # Making sure there are no holes in the weekly observations
        citizen.fill_care_holes()

        if len(citizen.falls) < cf.NUM_FALL_PRED:
            continue

        # Making the outcome - true if the fall-intensity is high enough, but not too high
        # (observation wont be included)
        pred_time, y, intensity = preprocessor.make_y_rec(citizen)
        if y is None or intensity > 0.5:
            continue

        x = preprocessor.make_x_rec(citizen, pred_time)
        if x is None:
            continue

        Y.append(y)
        G.append(citizen.get_constants(pred_time, sex_dict, organisation_dict,
                                       tuple(citizen.falls[cf.NUM_FALL_PRED - 2])))
        X.append(x)
        I.append(intensity)

    X = pad_sequences(X)
    Y = np.array(Y)
    G = np.array(G)

    print(f"\nData set made with a total of {len(Y)} observations of people that are falling at least \n"
          f"{cf.NUM_FALL_PRED} times. Out of these, {Y.sum()} are considered to be 'fallers' because \n"
          f"they fall with an intensity of more than {cf.INTENSITY_THRESHOLD} per year.\n")

    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'X_rec.pkl'), X)
    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'G_rec.pkl'), G)
    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'Y_rec.pkl'), Y)
