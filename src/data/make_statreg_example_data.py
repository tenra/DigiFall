"""
In this file some example citizens will be generated in order to calculate the probability of falling and show how it \
can be decreased.
"""
import src
from pathlib import Path
import numpy as np
import pandas as pd
from src import config as cf

# Loading the care dictionary
care_dict = {k: v for v, k in dict(enumerate(np.delete(
    src.tools.reader.load_pickle(Path.joinpath(cf.PROCESSED_DATA_DIR, 'care_types.pkl')), 51))).items()}


# Function for getting the index of the given care type (adding (FSIII))
def get_ind(care_type: str):
    return care_dict[care_type + " (FSIII)"]


def make_x(const, now_, before_):
    return np.append(np.append(const, now_), before_)


# Loading the pca-transformer used for training
pca = src.tools.reader.load_pickle(Path.joinpath(cf.READY_DATA_DIR, 'PCA_transformer.pkl'))
pca.choose_num_comp(8)

example_citizens = []
consts = [1, 80, 1, 0]

# %% Person 1 og 2 - Has a rise in component 8 vs not
# NOTE: Getting no home-care at all means that PC1 is automatically low (-1.5)
care_t4 = np.zeros(len(care_dict))
care_t4[get_ind('Sondeernæring')] = 0
care_t4[get_ind('Mobilitet')] = 50
care_t4[get_ind('Tøjvask')] = 0
care_t4[get_ind('Rengøring')] = 0
before = pca.make_score(care_t4)

care_t0 = np.zeros(len(care_dict))
# High in PC8
care_t0[get_ind('Sondeernæring')] = 30.0
care_t0[get_ind('Mobilitet')] = 200.0
care_t0[get_ind('Tøjvask')] = 40.0
care_t0[get_ind('Rengøring')] = 40.0
now = pca.make_score(care_t0)

example_citizens.append(make_x(consts, now, before))
# 2nd person does not
example_citizens.append(make_x(consts, before, before))

# %% Person 3 og 4 - What happens when PC3 has different values in the last week?
# High in PC3
care_t0 = np.zeros(len(care_dict))
care_t0[get_ind('RH Mobilitet')] = 50.0
care_t0[get_ind('Tilberede/anrette mad')] = 70.0
care_t0[get_ind('Udskillelser')] = 20.0
high = pca.make_score(care_t0)
# low in PC3
care_t0 = np.zeros(len(care_dict))
care_t0[get_ind('Personlig hygiejne')] = 300.0
care_t0[get_ind('Udskillelser')] = 200.0
care_t0[get_ind('Mobilitet')] = 200.0
low = pca.make_score(care_t0)

example_citizens.append(make_x(consts, low, low))
example_citizens.append(make_x(consts, low, high))

# %% Person 5 og 6 - What happens when PC1 changes
care_t0 = np.zeros(len(care_dict))
care_t0[get_ind('Personlig hygiejne')] = 200.0
care_t0[get_ind('Udskillelser')] = 150.0
care_t0[get_ind('Ernæring')] = 20.0
now = pca.make_score(care_t0)

care_t4 = np.zeros(len(care_dict))
care_t4[get_ind('Udskillelser')] = 0
care_t4[get_ind('Ernæring')] = 0
care_t4[get_ind('Indkøb')] = 0
care_t4[get_ind('Tøjvask')] = 0
care_t4[get_ind('Rengøring')] = 0
before = pca.make_score(care_t4)

example_citizens.append(make_x(consts, now, now))
example_citizens.append(make_x(consts, now, before))

# %% Person 4 - What happens when PC5 changes
care_t0 = np.zeros(len(care_dict))
care_t0[get_ind('Hverdagens aktiviteter')] = 50.0
care_t0[get_ind('RH Tilberede/anrette mad')] = 20.0
now = pca.make_score(care_t0)

care_t4 = np.zeros(len(care_dict))
care_t4[get_ind('Medicinadministration')] = 20.0
care_t4[get_ind('Medicindispensering')] = 20.0
care_t4[get_ind('Kompressionsbehandling')] = 20.0
before = pca.make_score(care_t4)

example_citizens.append(make_x(consts, now, before))
example_citizens.append(make_x(consts, before, now))

# %% Gathering everything
X = np.stack(example_citizens)
Y = np.zeros(len(X))
columns = [f'W1_PC{i+1}' for i in range(cf.NUM_components)] + [f'W2_PC{i+1}' for i in range(cf.NUM_components)]
df = pd.DataFrame(X, columns=['time', 'age', 'Male', 'Female'] + columns)
df['Y'] = Y
save_path = Path.joinpath(cf.READY_DATA_DIR, 'poisson_example_data.csv')
df.to_csv(save_path, sep=";")
print(f'CSV file saved to {save_path}')
