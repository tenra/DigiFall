from src.utility.data_utils import Citizen, year_week2date
from typing import List
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib
import numpy as np
matplotlib.rc('xtick', labelsize=7)


def fall_timeline(citizens: List[Citizen]) -> plt.Figure:
    """
    Visualize timelines for multiple citizens in one figure.

    :param citizens: List of citizens
    :return: figure
    """
    if type(citizens) == Citizen:
        citizens = [citizens]
    n = len(citizens)
    date_line = citizens[np.argmax([len(c.dates) for c in citizens])].dates  # Taking dates from the longest sequence
                                                                             # (might not be the best)
    fig, axes = plt.subplots(n, 1)
    for i, c in enumerate(citizens):
        c.show_timeline(ax=axes[i])
        if i != len(citizens) - 1:
            axes[i].axis('off')
        lgd = axes[i].get_legend()
        if lgd is not None:
            lgd.remove()
        axes[i].set_xlim(year_week2date(*date_line[0]), year_week2date(*date_line[-1]))
    axes[-1].get_yaxis().set_visible(False)
    axes[-1].spines["top"].set_visible(False)
    axes[-1].spines["right"].set_visible(False)
    axes[-1].spines["bottom"].set_visible(False)
    axes[-1].spines["left"].set_visible(False)

    custom_lgd = [Line2D([0], [0], marker='x', color='w', markeredgecolor='red', label='Fall'),
                  Line2D([0], [0], marker='o', color='w', markerfacecolor='black', label='Death')]
    axes[0].legend(handles=custom_lgd, prop={'size': 7})
    fig.show()
    return fig
