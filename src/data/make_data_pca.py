"""
This file makes the data but uses PCA scores instead of actual home-care observations. The outcome is binary \\
(if the fall-intensity is high the next period or not).
"""
from src.tools import reader, writer, preprocessor
from src.utility.PCA import PrincipalComponentAnalysis
from tensorflow.keras.preprocessing.sequence import pad_sequences
import numpy as np
from pathlib import Path
import src.config as cf
from tqdm import tqdm

if __name__ == "__main__":
    NUM_components = 16  # Number of components from the PCA should be used
    citizens, sex_dict, organisation_dict = reader.load_citizens()

    # Doing a PCA analysis on all the home-care observations simultaneously
    HC = [c.home_care[:, 2:] for c in citizens]
    HC = np.concatenate(HC)
    HC = np.delete(HC, 51, axis=1)  # Removing column 51 (Forflytning og Mobilisering) because we have no obs.

    pca = PrincipalComponentAnalysis(30)
    pca.fit(HC)
    pca.draw_scree_plots(K=50)
    pca.choose_num_comp(NUM_components)
    comps = pca.get_components()  # The individual components can be read from this dataframe along with the care-dict.

    X, G, Y = [], [], []

    for citizen in tqdm(citizens):

        citizen.fill_care_holes()

        if len(citizen.falls) < cf.NUM_FALL_PRED:
            continue

        pred_time, y, intensity = preprocessor.make_y_rec(citizen)
        if y is None or intensity > 0.5:
            continue

        x = preprocessor.make_x_rec(citizen, pred_time)
        if x is None:
            continue

        x = np.delete(x[:, :-1], 51, axis=1)

        # Making the individual PCA scores for the different observations in time and stacks them
        xs = []
        for week in x:
            xs.append(pca.make_score(week))

        X.append(np.stack(xs))
        Y.append(y)
        G.append(citizen.get_constants(pred_time, sex_dict, organisation_dict))

    X = pad_sequences(X, dtype='float')
    Y = np.stack(Y)
    G = np.stack(G)

    print(f"\nData set made with a total of {len(Y)} observations of which {np.sum(Y)} are considered to be 'fallers'. "
          f"\n")
    # Saves the data ready for modelling and the PCA_transformer object in order to use it on unseen data
    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'X_pca.pkl'), X)
    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'G_pca.pkl'), G)
    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'Y_pca.pkl'), Y)
    writer.pickle_obj(Path.joinpath(cf.READY_DATA_DIR, 'PCA_transformer.pkl'), pca)
