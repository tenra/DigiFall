from abc import ABC
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import GRU, Dense, Dropout, concatenate, Masking
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.metrics import AUC


class multioutcome_RNN(Model, ABC):
    """
    The multiclass RNN outputting both a probability of falling and of dying independently of each other. Note that the\
    targets input into the model should be named as the values returned.
    Notice! That the individual layer for the 'dying' part is given less nodes (factor of X) in order to prevent over\
    -fitting

    :param GRU1_units: the number of units in the first recurrent layer (can be 0 if no layers is wanted)
    :param GRU_units: the number of units in the 2nd recurrent layer (outputting single values)
    :param common_lin_units: the number of units in the shared dense layer (can be 0 if no layer is wanted)
    :param lin_fall: the number of units in the dense layer modelling the falling outcome
    :param lin_die: the number of units in the dense layer modelling the dying outcome
    :param dropout: the dropout value to be used throughout the model
    """
    def __init__(self, GRU1_units=0, GRU_units=10, common_lin_units=9, lin_fall=0, lin_die=0, dropout=0.4):
        super(multioutcome_RNN, self).__init__()

        self.two_rec = GRU1_units != 0
        self.common_lin = common_lin_units != 0
        self.has_lin_fall = lin_fall != 0
        self.has_lin_die = lin_die != 0

        if self.two_rec:
            self.gru1 = GRU(units=GRU1_units, return_sequences=True, activation='relu', dropout=dropout,
                            recurrent_dropout=dropout)
            self.BN_gru1 = BatchNormalization()
        self.gru2 = GRU(units=GRU_units, return_sequences=False, activation='relu', dropout=dropout,
                        recurrent_dropout=dropout)
        self.BN_gru2 = BatchNormalization()

        if self.common_lin:
            self.lin1 = Dense(units=common_lin_units)
            self.BN_lin1 = BatchNormalization()

        if self.has_lin_fall:
            self.lin_fall1 = Dense(units=lin_fall, activation='relu')
            self.BN_lin_fall1 = BatchNormalization()
        if self.has_lin_die:
            self.lin_die1 = Dense(units=lin_die, activation='relu')
            self.BN_lin_die1 = BatchNormalization()

        self.lin_fall2 = Dense(units=1, activation='sigmoid')
        self.lin_die2 = Dense(units=1, activation='sigmoid')

        self.dropout = Dropout(rate=dropout)

    def call(self, inputs, training=False, mask=None):
        x, g = inputs

        if self.two_rec:
            x = self.gru1(x, training=training)
            x = self.BN_gru1(x, training=training)
        x = self.gru2(x, training=training)
        x = self.BN_gru2(x, training=training)

        # Only inputting the constants after the recurrent layers
        x = concatenate((x, g))

        if self.common_lin:
            x = self.dropout(self.lin1(x)) if training else self.lin1(x)
            x = self.BN_lin1(x, training=training)

        if self.has_lin_fall:
            x_fall = self.dropout(self.lin_fall1(x)) if training else self.lin_fall1(x)
            x_fall = self.BN_lin_fall1(x_fall, training=training)
        else:
            x_fall = x
        if self.has_lin_die:
            x_die = self.dropout(self.lin_die1(x)) if training else self.lin_die1(x)
            x_die = self.BN_lin_die1(x_die, training=training)
        else:
            x_die = x

        return {'falling': self.lin_fall2(x_fall), 'dying': self.lin_die2(x_die)}


def create_model(learning_rate=0.01,
                 GRU1_units=0,
                 GRU_units=10,
                 common_lin_units=0,
                 lin_fall=10,
                 lin_die=10,
                 dropout=0.4) -> keras.Model:
    """
    Function that creates and compiles the multi-outcome recurrent neural network.

    :param learning_rate: Learning rate to be used in the Adam optimizer
    :param GRU1_units: Number of units in the first recurrent layer (0 implies no layer)
    :param GRU_units: Number of units in the 2nd recurrent layer (or first)
    :param common_lin_units: Number of units in the shared linear layer (0 implies no layer)
    :param lin_fall: Number of units in the linear layer only for the falling outcome (0 implies no layer)
    :param lin_die: Number of units in the linear layer only for the dying outcome (0 implies no layer)
    :param dropout: percentage to be used as dropout throughout the model
    :return: A complied multi-outcome RNN
    """
    res_model = multioutcome_RNN(GRU1_units=int(GRU1_units),
                                 GRU_units=int(GRU_units),
                                 common_lin_units=int(common_lin_units),
                                 lin_fall=int(lin_fall),
                                 lin_die=int(lin_die),
                                 dropout=dropout)
    metrics = ['accuracy', AUC(name='auc'), AUC(name='prauc', curve='PR')]
    optimizer = keras.optimizers.Adam(learning_rate=learning_rate)
    losses = {'falling': 'binary_crossentropy', 'dying': 'binary_crossentropy'}
    loss_weights = {'falling': 1.0, 'dying': 1.0}
    res_model.compile(loss=losses, loss_weights=loss_weights, optimizer=optimizer, metrics=metrics)
    return res_model


class RNN(Model, ABC):
    """
    The RNN model that is used to classify falls.

    :param GRU_units: The number of hidden units in the recurrent layers
    :param lin_units: The number of units in the dense layers
    """

    def __init__(self, GRU_units, lin_units, dropout=0.4):
        """
        Initializer.
        """
        super(RNN, self).__init__()

        self.gru1 = GRU(units=GRU_units, return_sequences=True, activation='relu', dropout=dropout, recurrent_dropout=dropout)
        self.BN_gru1 = BatchNormalization()

        self.gru2 = GRU(units=GRU_units, return_sequences=False, activation='relu', dropout=dropout, recurrent_dropout=dropout)
        self.BN_gru2 = BatchNormalization()

        self.lin1 = Dense(units=lin_units, activation="relu")
        self.BN_lin1 = BatchNormalization()

        self.lin2 = Dense(units=1, activation="sigmoid")

        self.mask = Masking(mask_value=0)
        self.dropout = Dropout(dropout)

    def call(self, inputs, training=False, mask=None):
        x, g = inputs

        x = self.mask(x)  # Ignoring padded zeros

        x = self.gru1(x, training=training)
        x = self.BN_gru1(x, training=training)
        x = self.gru2(x, training=training)
        x = self.BN_gru2(x, training=training)

        x = concatenate((x, g))

        x = self.dropout(self.lin1(x)) if training else self.lin1(x)
        x = self.BN_lin1(x, training=training)

        return self.lin2(x)


class RNN_REG(Model, ABC):
    """
    The regression RNN made in order to try predicting the number of falls the next 3 months.

    :param GRU_units: The number of units in the recurrent layers
    :param lin_units: THe number of units in the linear or dense layers
    :param dropout: The dropout parameter to be used (Default=0.7)
    """

    def __init__(self, GRU_units, lin_units, dropout=0.7):
        super(RNN_REG, self).__init__()

        self.gru1 = GRU(units=GRU_units, return_sequences=True, activation='relu', dropout=dropout,
                        recurrent_dropout=dropout)
        self.BN_gru1 = BatchNormalization()

        self.gru2 = GRU(units=GRU_units, return_sequences=False, activation='relu', dropout=dropout,
                        recurrent_dropout=dropout)
        self.BN_gru2 = BatchNormalization()

        self.lin1 = Dense(units=lin_units, activation="relu")
        self.BN_lin1 = BatchNormalization()

        self.lin2 = Dense(units=1, activation="relu")

        self.mask = Masking(mask_value=0)
        self.dropout = Dropout(dropout)

    def call(self, inputs, training=False, mask=None):
        x, g = inputs

        x = self.mask(x)  # Ignoring padded zeros

        x = self.gru1(x, training=training)
        x = self.BN_gru1(x, training=training)
        x = self.gru2(x, training=training)
        x = self.BN_gru2(x, training=training)

        x = concatenate((x, g))

        x = self.dropout(self.lin1(x)) if training else self.lin1(x)
        x = self.BN_lin1(x, training=training)

        return self.lin2(x)
