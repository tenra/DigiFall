"""
This sub-package holds all the RNN model-classes. The create_model function is to be used in the nested \
cross-validation. The function takes a configuration input and initialises and compiles a model.
"""
from .RNNs import *
