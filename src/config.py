"""
This file has all configurations for doing the data preprocessing and all the relevant locations to read / write from.
"""

from pathlib import Path

# %% Directories
ROOT_DIR = Path(__file__).absolute().parent.parent

# Raw data
RAW_DATA_DIR = Path.joinpath(ROOT_DIR, "data/raw/")
FILENAMES_RAW = {'P83': '83.xlsx', 'P83a': '83a.xlsx', 'P86': '86.xlsx', 'P140': '140.xlsx',
                 'fall_AIR': 'AIR_FALd.xlsx', 'home_care': 'Digi_06-2019_til_02-2021.xlsx',
                 'fall': 'Faldudredning.xlsx'}

# Processed data
PROCESSED_DATA_DIR = Path.joinpath(ROOT_DIR, 'data/processed')

# Data ready for modeling
READY_DATA_DIR = Path.joinpath(ROOT_DIR, 'data/ready')
FILENAMES_READY_FIRST = {'X': 'X_first.pkl', 'Y': 'Y_first.pkl', 'G': 'G_first.pkl'}
FILENAMES_READY_REC = {'X': 'X_rec.pkl', 'Y': 'Y_rec.pkl', 'G': 'G_rec.pkl'}
FILENAMES_READY_PCA = {'X': 'X_pca.pkl', 'Y': 'Y_pca.pkl', 'G': 'G_pca.pkl'}
FILENAMES_READY_PCA_REG = {'X': 'X_pca_reg.pkl', 'Y': 'Y_pca_reg.pkl', 'G': 'G_pca_reg.pkl'}

# Trained models
MODELS_DIR = Path.joinpath(ROOT_DIR, 'models')
FILE_NAMES_MODELS = {'RNN': 'trained_RNN', 'multiRNN': 'trained_multiRNN', 'multiRNN2': 'trained_multiRNN2'}

# References
REFERENCES_DIR = Path.joinpath(ROOT_DIR, 'references')
FILENAMES_REF = {'home_care': 'care_dict.pkl', 'organisation': 'organisation_dict.pkl', 'sex': 'sex_dict.pkl'}

# %% Modelling configurations
"""
The configurations for preprocessing the data for statistical analysis
"""
LOOK_BACK = 4  # Number of weeks to consider before the final week
LOOK_FORWARD = 12  # Number of weeks to count falls in
NUM_components = 8  # Number of components from the PCA should be used
NUM_FALL_PRED = 2

"""
The configurations for making the recurrent-faller network with an intensity threshold
"""
NUM_FALLS_MAX = 5
INTENSITY_THRESHOLD = 4  # Number of falls per year

"""
The configurations for doing basic preprocessing and the right alignment for the RNN
"""
PREDICTION_WINDOW = 12  # Number of weeks before the event, the prediction should take place
LAST_WEEK_DEATH_LIM = 300  # If you go from more than this to 0, you are considered fall-out
INCLUDE_AIR_FALLS = True

FIRST_WEEK = (2019, 23)
LAST_WEEK = (2021, 8)
