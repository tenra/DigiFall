DigiFall Python Source Code
===========================
The python source code have been used to do all data preprocessing and all analyses using 
recurrent neural networks. All code have been documented and the documentation files are in the 
*docs* folder in the parent directory.

#### Required Python Packages
The packages mentioned below are neccesary in order to properly run the python code:

- numpy 1.19.5
- tenserflow 2.5.0
- matplotlib 3.4.2
- pandas 1.3.0
- scikit-learn 0.24.2

Below the course of action for making the data and doing the analysis will be described.

Assembling the Data for Modelling
---------------------------------
In order to make the data for modelling, first run the *clean_data.py* file in the *data* sub-package. This assembles all
the citizens in a list and makes also the different dictionaries in order to find the index of
specific type of care, sex, or organisation. Afterwards the individual files *make_data_--.py*
can be run to make the data ready for modelling. Each of these makes 3 data arrays: X (3-way 
tensor used for all the observations that differs with time), G (matrix used for static variables
as for instance sex and age), and Y (array of the different outcomes to be modelled). Each of the 
data files will have a suffix corresponding to what model type it is under (for instance 'pca', or
'first'). Ones the data is made ready for modelling, the data can be loaded using the *Data* class
in the src.tools.data_loader sub-package.

#### Reading the PCA component weights
In order to find out which types of home-care are weighted by the individual components, the PCA-class has a function
that returns a pandas data-frame with all this information. See the PCA documentation for details.

#### Data for Statistical Regression 
In order to make the data for statistical regression, run the *make_poisson_data.py* file. This
will take the processed data and make a single .csv file with each column being a variable or outcome
and each row an observation. The file will be called "poisson_data.csv" and can be read from R.

Running the Analyses
--------------------
All the different files in the *analysis* sub-package can be run in order to train and assess the
performance of each of the different neural network models tried in this project.
