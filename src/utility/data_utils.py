from datetime import datetime, timedelta, date
from typing import Union, List, Tuple
import pandas as pd
import numpy as np
from collections import defaultdict
import src.config as cf
import matplotlib.pyplot as plt


class Citizen:
    """
    Class for holding a single citizen including home-care history and general information such as gender and ID. \
    The home-care history is an array of shape (weeks x (care_types + 2)) where the two first columns correspond to \
    the year and week numbers and where the rest is the number of minutes of care given in a given week of a given \
    type.

    :param df: Dataframe containing all the observations related to the given individual
    :param care_dict: The care-type dictionary containing all the types of home-care.
    """

    def __init__(self, df: pd.DataFrame, care_dict, minute_lim=cf.LAST_WEEK_DEATH_LIM):
        ID, sex, birth_year, organisation = df[['ID', 'sex', 'birth_year', 'Organisation']].iloc[0]
        self.dates, self.date_dict = make_date_dict(df)
        self.ID = ID
        self.sex = sex
        self.birth_year = birth_year
        self.organisation = organisation
        self.first_week, self.last_week = self.dates[0], self.dates[-1]
        self.length_care = self.dates.shape[0]
        self.home_care = make_care_variables(df, self.dates, self.date_dict, care_dict)
        self.dead = self.update_death(lim=minute_lim)
        self.falls = []

    def update_death(self, lim=0):
        """
        Updates the data for the citizen to include the death of the person if the home care goes to 0 after having \
        been above "lim" the previous week.

        :return: boolean specifying if the person dies in self.last_week or not.
        """
        for i in range(1, self.length_care):
            last = self.home_care[i - 1, 2:].sum()
            before = self.home_care[:i, 2:].sum()
            after = self.home_care[i:, 2:].sum()
            if after == 0 and before > 0 and last > lim:
                self.home_care = self.home_care[:i]
                self.dates = self.dates[:i]
                self.last_week = self.dates[-1]
                return True
        if year_week_diff(self.last_week, cf.LAST_WEEK) < 0 and self.home_care[-1, 2:].sum() > lim:
            return True
        return False

    def add_fall(self, week, check_duplicates=False):
        """
        Adds a fall on the date given to the history of the citizen and sorts the array so that the earliest fall in
        is also the first fall in the falls-array.

        :param check_duplicates: Boolean variable to check whether the fall has already been registered.
        :param week: the date on which the fall happened
        """
        # Add? Checking if the fall is already present
        if next((False for elem in self.falls if np.array_equal(elem, np.array(week))), True) \
                or check_duplicates is False:
            self.falls.append(week)
            falls = np.array(self.falls)
            falls = falls[falls[:, 1].argsort()]
            falls = falls[falls[:, 0].argsort(kind="stable")]
            self.falls = list(falls)

    def falls_between(self, first, end=None):
        """
        Function that returns the number of falls from (not including) first to (including) end.

        :param first: The first week in period
        :param end: The last week in period
        :return: list of falls
        """
        if end is None:
            for i, fall in enumerate(self.falls):
                if year_week_diff(first, fall) < 0:
                    return self.falls[i:]
        falls = []
        for i, fall in enumerate(self.falls):
            if year_week_diff(first, tuple(fall)) < 0 and year_week_diff(tuple(fall), end) <= 0:
                falls.append(fall)
        return falls

    def fill_care_holes(self):
        """
        Fills the gaps in the weekly home-care observations assuming that no observation means no home-care (0 min) is
        given for every type.
        """
        for i in range(len(self.dates) - 1, 0, -1):
            diff = year_week_diff(self.dates[i], self.dates[i - 1])
            for _ in range(diff - 1):
                new = get_later_week(self.dates[i], -1)
                self.home_care = np.insert(self.home_care, i, 0, axis=0)
                self.home_care[i, 0:2] = new
                self.dates = np.insert(self.dates, i, new, axis=0)
        # Updating the dictionary
        new_dict = defaultdict()
        for i, week in enumerate(self.dates):
            new_dict[tuple(week)] = i
        self.date_dict = new_dict
        self.length_care = self.dates.shape[0]

    def show_timeline(self, line: Union[np.ndarray, None] = None, ax: Union[plt.Axes, None] = None):
        """
        Shows the timeline of the citizen with a red x corresponding to a fall and a black dot corresponding to the
        citizen dying (going to 0 total home-care)

        :param line: A list of dates if the timeline should be aligned with those of other patints.
        :param ax: an matplotlib-ax element if used in a subfigure
        """
        if ax is None:
            fig, ax = plt.subplots(1, 1)
        line = self.dates if line is None else line
        ax.plot(year_week2date(line[:, 0], line[:, 1]), np.zeros(len(line)))
        if len(self.falls) > 0:
            falls = np.array(self.falls)
            dates = year_week2date(falls[:, 0], falls[:, 1])
            ax.scatter(dates, np.zeros(len(dates)), marker="x", color="red", label="Fall")
        if self.dead:
            ax.scatter([year_week2date(self.last_week[0], self.last_week[1])], [0], marker='o', color='black',
                       label="Death")
        if len(ax.get_legend_handles_labels()[0]) > 0:
            ax.get_yaxis().set_visible(False)
            ax.legend()
        plt.show()

    def num_fall(self):
        """
        Returning the number of falls experienced by the citizen in the data.

        :return: int
        """
        return len(self.falls)

    def get_age(self, year: int) -> int:
        """
        Returns the age of the citizen in a given year, based on how old they turn that year.

        :return: age as an integer
        """
        return year - int("19" + str(self.birth_year))

    def get_constants(self, pred_time, sex_dict, organisation_dict, time_before: Tuple[int, int] = None) -> np.ndarray:
        """
        Returns the constants one-hot encoded data. The first format is week number, age, sex, organisation

        :param organisation_dict: Dictionary of organisation types
        :param sex_dict: Dictionary of sex types
        :param pred_time: the time of prediction
        :param time_before: include if the function should also return the difference to this date (previous fall)
        :return: numpy aray
        """
        sex = np.zeros(len(sex_dict))
        sex[sex_dict[self.sex]] = 1
        organisation = np.zeros(len(organisation_dict))
        organisation[organisation_dict[self.organisation]] = 1
        if time_before is None:
            return np.array([pred_time[1], self.get_age(pred_time[0]), *sex, *organisation])
        time_diff = year_week_diff(pred_time, time_before)
        return np.array([pred_time[1], self.get_age(pred_time[0]), *sex, *organisation, time_diff])


def make_date_dict(df: pd.DataFrame) -> (np.ndarray, dict):
    """
    Takes a dataframe and makes a dictionary of all different weeks and an array of all the year-week pairs. Both \
    are sorted.

    :param df: Dataframe of various lengths
    :return: array of dates and corresponding dictionary
    """
    dates = df[['year', 'week']].drop_duplicates().to_numpy()
    dates = dates[dates[:, 1].argsort()]
    dates = dates[dates[:, 0].argsort(kind='stable')]
    date_dict = defaultdict()
    for i, week in enumerate(dates):
        date_dict[tuple(week)] = i
    return dates, date_dict


def make_type_dict(types: List[str]) -> dict:
    """
    Makes a dictionary of all the different types of home-care.

    :param types: list of string (names) or equivalent.
    :return: care-type dictionary
    """
    type_dict = defaultdict()
    for i, type_ in enumerate(types):
        type_dict[type_] = i
    return type_dict


def make_care_variables(df: pd.DataFrame, dates: np.ndarray, date_dict: dict, care_dict: dict) -> np.ndarray:
    """
    Encodes all observations for a single person to a sequence of variables containing the number of minutes of care \
    received of that type.

    :param df: All observations for a single person (ID)
    :param dates: (n x 2) array of all year-week pairs for which the given person is receiving care.
    :param date_dict: the corresponding date-dictionary
    :param care_dict: the care-dictionary
    :return: An array where the two first columns are the year and week and the rest correspond to each type of care \
    elements are number of minutes of care received in that week of that type of care.
    """
    minutes_care = np.zeros((len(dates), len(care_dict) + 2))
    minutes_care[:, 0:2] = dates
    for year, week, type_, min_ in df[['year', 'week', 'care_type', 'minutes']].to_numpy():
        minutes_care[date_dict[(year, week)], care_dict[type_] + 2] = min_
    return minutes_care


def year_week2date(year: Union[int, List[int]], week: Union[int, List[int]]) -> Union[date, List[date]]:
    """
    Takes in a year and a week number and returns a date-variable.

    :param year: int / list
    :param week: int / list
    :return: date variable(s)
    """
    if type(year) == list or type(year) == np.ndarray:
        dates = []
        for i, y in enumerate(year):
            off = 0 if y == 2021 else 1
            dates.append(datetime.strptime(f'{y}-W{week[i] - off}-1', "%Y-W%W-%w").date())
        return dates
    else:
        off = 0 if year == 2021 or year == 2018 else 1
        return datetime.strptime(f'{year}-W{week - off}-4', "%Y-W%W-%w").date()


def date_after(week1: Tuple[int, int], week2: Tuple[int, int]) -> bool:
    """
    Assesses whether the week1 is after week2

    :param week1: (year, week)
    :param week2: (year, week)
    :return: boolean
    """
    diff = year_week_diff(week1, week2)
    if diff > 0:
        return True
    return False


def year_week_diff(date1: Tuple[int, int], date2: Tuple[int, int]) -> int:
    """
    Calculates the difference between week1 and week2 in the number of weeks.

    :param date1: (year, week)
    :param date2: (year, week)
    :return: int
    """
    date1 = year_week2date(*date1)
    date2 = year_week2date(*date2)
    diff = date1 - date2
    return diff.days // 7


def get_later_week(week: Tuple[int, int], week_diff: int) -> Tuple[int, int]:
    """
    Returns a (year, week) object that is week_diff weeks away from date.

    :param week: intitial date (year, week)
    :param week_diff: week difference
    :return: new date
    """
    week = year_week2date(*week)
    week = week + timedelta(weeks=week_diff)
    return week.year, week.isocalendar()[1]
