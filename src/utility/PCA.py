from typing import Tuple
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


class PrincipalComponentAnalysis:
    """
    Class for doing principal component analysis and evaluate the quality / configuration. The PrincipalComponentAnalysis \\
    object consists of first a standardisation of the data and then the PCA itself. Both transformations are saved and \\
    can be used afterward on unseen data.

    :param n_components: The number of components to be fitted. Should be 0 < n_components <= p, where p is the number \\
    of variables in the data. Defaults to 10.
    """

    def __init__(self, n_components=10):
        self.fitted = False
        self.n, self.m = None, None
        self.scaler = StandardScaler()
        self.transformer = PCA(n_components=n_components)
        self.num_chosen = n_components

    def fit(self, X: np.ndarray):
        """
        Fits the data by initially standardizing and then fitting the components. Both transformers are saved in the \\
        PCA object.

        :param X: data to be fitted
        :return:
        """
        self.fitted = True
        self.n, self.m = X.shape
        X = self.scaler.fit_transform(X)
        self.transformer.fit(X)

    def make_score(self, x: np.ndarray) -> np.ndarray:
        """
        Outputs the PCA scores of previously unseen data using the fitted standardization and PCA components.

        :param x: a single observation of all p variable values.
        :return: PCA scores for the number of components chosen
        """
        if self.fitted is False:
            raise UserWarning('The PCA algorithm have not yet been fit')

        x = self.scaler.transform(np.expand_dims(x, 0))
        x = self.transformer.transform(x)
        return x[0, :self.num_chosen]

    def draw_scree_plots(self, K: int = 20) -> Tuple[plt.Figure, plt.Axes, plt.Axes]:
        """
        Drawing the scree plot and cumulated variance explained of the PCA components. Also prints how many components \\
        to choose according to the two criteria; eigenvalue below 1 or eigenvalue below randomly generated line.

        :param K: How many random data points to generate. More data-point yields better results, but is also slower.
        :return: plt.fig object and both ax-objects.
        """
        if self.fitted is False:
            raise UserWarning('The PCA algorithm have not yet been fit')

        PCA_rand = PCA(n_components=self.transformer.n_components)
        sumCompEig = np.zeros(PCA_rand.n_components)
        for i in range(K):
            PCA_rand.fit(np.random.normal(0, 1, size=(self.n, self.m)))
            sumCompEig += PCA_rand.explained_variance_
        sumCompEig /= K

        pca_values = np.arange(self.transformer.n_components) + 1
        fig, [ax, ax2] = plt.subplots(1, 2)
        ax.plot(pca_values, self.transformer.explained_variance_, marker='o', label='From data')
        ax.plot(pca_values, PCA_rand.explained_variance_, '--', label='Randomly generated')
        ax.plot([0, self.transformer.n_components], [1, 1], '--', label='Value of 1')
        ax.set(xlabel='Component', ylabel='Eigenvalue', title='Scree plot')
        ax.legend()

        ax2.plot(pca_values, np.cumsum(self.transformer.explained_variance_ratio_))
        ax2.set(xlabel='Component', ylabel='Cum. Ratio of Var.', title='Cum. Prop. Explained Var.')
        fig.show()
        print(f'Component no. {np.where(self.transformer.explained_variance_ < 1)[0][0] + 1} is the first to have an '
              f'eigenvalue below 1. Component no. '
              f'{np.where(self.transformer.explained_variance_ < PCA_rand.explained_variance_)[0][0] + 1} is the first '
              f'to be below the randomly generated line.')

        return fig, [ax, ax2]

    def choose_num_comp(self, n: int):
        """
        Function for choosing the number of components to be used. (e.g. chosen based on the scree-plot)

        :param n: The number of chosen components. Should be > 0 and less than the number of components fitted
        """
        if n > self.transformer.n_components:
            raise ValueError('The chosen number of components cannot be greater than the number of components fitted. '
                             'The transformer needs to be fitted again.')
        elif n < 1:
            raise ValueError('The chosen number of components needs to be at least 1.')
        self.num_chosen = n

    def get_components(self) -> pd.DataFrame:
        """
        Returning the components of the PCA as a pandas.DataFrame. Explains how much each of the components weigh the \\
        values of the parameters.

        :return: data frame of component weights
        """
        if self.fitted is False:
            raise UserWarning('The PCA algorithm have not yet been fit')
        names = []
        for i in range(self.num_chosen):
            names.append(f'PC{i+1}')
        df = pd.DataFrame(self.transformer.components_[:self.num_chosen].T, columns=names)
        return df
